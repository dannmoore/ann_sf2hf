--[[
  File:    ANN_GA.lua
  Description: A simple genetic algorithm implementation
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
--]]


--
-- randomFloat() - Generate a random floating point number
--  Note that this generates a number between [lower, upper), that is it will come close to but never exactly equal upper
--
function randomFloat(lower, upper)
	return lower + (math.random() * (upper - lower))
end





--
-- Genetic Algorithm functions
--
local AI_GA = {}


--
-- AI_GA:new() - Creeate an AI_GA "object" and return it
--
function AI_GA:new()
	local ga = {fitness = {}, genomes = {}, parents = {}}
	setmetatable(ga, self)
	self.__index = self
	return ga
end



--
-- AI_GA:StoreFitness - Store an input table containing fitness data for several genomes
-- 
function AI_GA:StoreFitness(f)
	self.fitness = f
end


--
-- AI_GA:StoreGenomes - Store an input table containing serialized genome data for several genomes
--
function AI_GA:StoreGenomes(g)
	self.genomes = g
end


--
-- AI_GA:BreedFromParents - Returns a new genome given two parent genomes 
--
function BreedFromParents(p1, p2, mutationrate)
	local newgenome = {}

	for i = 1, #p1 do
		-- For each value in the genome choose one parent at random to inherit from	
		if math.random(1,2) == 1 then
			newgenome[#newgenome + 1] = p1[i]
		else
			newgenome[#newgenome + 1] = p2[i]
		end

		-- Mutate at random chance	
		if math.random(1,10000) <= mutationrate then 			
			newgenome[#newgenome] = randomFloat(-1,1)
		end
	end

	return newgenome
end


--
-- AI_GA:Breed - Using the given genomes and fitness data, breeds a new generation and returns
--                the new genome data
--
function AI_GA:Breed(num_fitparents, num_randomparents, mutationrate)
	-- Clear out parents table
	self.parents = {}


	-- Select the most fit parents
	-- Start by sorting by highest fitness
	local newfitness = {}
	local newgenomes = {}
	local highest = self.fitness[1]
	local tmp = 0
	for i = 1, #self.fitness - 1 do	
		highest = i
		for j = i + 1, #self.fitness do
			if self.fitness[j] > self.fitness[highest] then
				highest = j
			end
		end

		if i ~= highest then
			-- swap elements
			tmp = self.fitness[i]
			self.fitness[i] = self.fitness[highest]
			self.fitness[highest] = tmp

			tmp = self.genomes[i]
			self.genomes[i] = self.genomes[highest]
			self.genomes[highest] = tmp
		end
	end


	-- Next select num_fitparents as parents for the next generation
	for i = 1, num_fitparents do
		self.parents[#self.parents + 1] = self.genomes[i]
	end

	-- Next select num_randomparents as parents for the next generation
	local indexes = {}
	local duplicate = false
	while #indexes < num_randomparents do
		rnd = math.random(num_fitparents + 1, #self.genomes)	-- Select randomly from the remaining genomes
		duplicate = false
		for j = 1, #indexes do
			if indexes[j] == rnd then -- Only add if it is not a duplicate index
				duplicate = true
			end
		end
		if not duplicate then
			indexes[#indexes + 1] = rnd
		end
	end

	for i = 1, #indexes do
		self.parents[#self.parents + 1] = self.genomes[indexes[i]]
	end




	-- Breed random pairs of parents until we get a new set of genomes
	local totalgenomes = #self.genomes
	self.genomes = {}
	for i = 1, totalgenomes do
		-- Select two parents at random
		index1 = math.random(1, #self.parents)
		index2 = math.random(1, #self.parents)
		while index1 == index2 do -- Require two unique parents
			index2 = math.random(1, #self.parents)
		end

		-- Breed a new genome from these parents
		self.genomes[#self.genomes + 1] = BreedFromParents(self.parents[index1], self.parents[index2], mutationrate)
	end

	-- Clear out fitness table for next time
	self.fitness = {}

	-- Return the new genomes for the next generation
	return self.genomes
end

return AI_GA