--[[
  File:    ANN_sf2hf.lua
  Description: A plugin for MAME that implements a pair of adversarial neural networks to play Street Fighter 2
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
--]]





--
-- Debug
--
-- Print main screen width/height
--print("Screen Width: " .. manager:machine().screens[":screen"]:width())
--print("Screen Height: " .. manager:machine().screens[":screen"]:height())





--
-- Global Data
--

-- Gather all buttons available
local buttons0 = {}
local buttons1 = {}
local buttons2 = {}
local switchesB = {}
local ports = manager:machine():ioport().ports
--for k,v in pairs(ports) do print(k) end

local IN0 = ports[":IN0"]
--print("Port 0 Button Names:")
for field_name, field in pairs(IN0.fields) do
    buttons0[field_name] = field
    --print(buttons0[field_name].name)
end
local IN1 = ports[":IN1"]
--print("Port 1 Button Names:")
for field_name, field in pairs(IN1.fields) do
    buttons1[field_name] = field
    --print(buttons1[field_name].name)
end
local IN2 = ports[":IN2"]
--print("Port 2 Button Names:")
for field_name, field in pairs(IN2.fields) do
    buttons2[field_name] = field
    --print(buttons2[field_name].name)
end

local DSWB = ports[":DSWB"]
--print("Dip Switches B Names:")
for field_name, field in pairs(DSWB.fields) do
    switchesB[field_name] = field
    --print(switchesB[field_name].name)
end



math.randomseed(os.time()) -- Seed RNG with OS time


-- A random amount to vary the game start.  This will vary any timer-based RNG in the emulated machine
local frame_randomstart = math.random(0,30) 
local frame_num = 0
local frame_throttle = 4  -- Throttle AI response to every N frames (reduces reaction time and attack spamming)
local game_started = false
local continue_game = false
local frame_continue = 0 -- Frame counter for continuing game
local select_character = false
local frame_select = 0 -- Frame counter for selecting a random character
local select_rnd1 = 0 -- Frame counter for selecting a random character
local select_rnd2 = 0 -- Frame counter for selecting a random character
local endround_processed = false	-- Has the end of the round been processed yet?

local P1Wins = 0	-- Number of Player 1 wins in match
local P2Wins = 0	-- Number of Player 2 wins in match
local Draws = 0	-- Number of draws in match





--
-- Game-related memory functions
--

-- Note that these memory addresses may change between versions of this game!
--
-- Game data begins @ 0xFF844E
-- Continue timer @ 0xFF8645, 1 byte, 0-9

function P1_Enabled()
	if manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF86EE) == 1 then
		return true
	end

	return false
end


function P2_Enabled()
	if manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF83EE) == 1 then
		return true
	end

	return false
end


function P1_GetHealth()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF83E8)
end

function P2_GetHealth()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF86E8)
end


function P1_GetPositionX()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF858A)
end

function P1_GetPositionY()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF858E)
end


function P2_GetPositionX()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF888A)
end

function P2_GetPositionY()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF888E)
end


--[[ Character Table:
	Ryu = 0x00
	E. Honda = 0x01
	Blanka = 0x02
	Guile = 0x03
	Ken = 0x04
	Chun Li = 0x05
	Zangief = 0x06
	Dhalsim = 0x07
	M. Bison = 0x08
	Sagat = 0x09
	Balrog = 0x0A
	Vega = 0x0B
--]]
function P1_GetCharacter()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF864F)
end

function P2_GetCharacter()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF894F)
end


--[[
	Character state table:
	0x00 = Standing
	0x02 = Crouching 
	0x04 = Jumping
	0x06 = Jump-crouch
	0x08 = Blocking
	0x0A = Attacking
	0x0E = Stunned
	0x14 = On Ground
--]]
function P1_GetState()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF83C1)
end

function P2_GetState()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF86C1)
end


-- There can be up to 8 projectiles at a time (slots 0-7)
-- The game uses slot 7 first, then slot 6, and soforth
function ProjectileActive(slotnum)
	if manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF9387 + (0xC0 * slotnum)) ~= 0 then
		return true
	else
		return false
	end
end

function ProjectilePositionX(slotnum)
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF937C + (0xC0 * slotnum))
end

function ProjectilePositionY(slotnum)
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF9380 + (0xC0 * slotnum))
end



function GetTimer()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF8ABE)
end



function GetRoundDone()
	if manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF8AC0) == 1 then
		return true
	end

	return false
end

-- 0 = no winner, 1 = Player 1, 2 = Player 2, 255 = Draw
function GetRoundWinner()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF8AC2)	
end


function P1_GetContinueTime()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF8645)	
end

function P2_GetContinueTime()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF8945)	
end

function ClearContinueTimes()
	manager:machine().devices[":maincpu"].spaces["program"]:write_u8(0xFF8645, 0)
	manager:machine().devices[":maincpu"].spaces["program"]:write_u8(0xFF8945, 0)
end




--
-- Neural network
--
local AI_NN = require("ANN_sf2hf/ANN_NN")
local P1_NN = {} -- Neural network for player 1
local P2_NN = {} -- Neural network for player 2
local sensitivity = 0.70 -- How much of a signal on the output nodes do we need to activate a given button
local combo_move_bias = 0.10 -- How much bias to apply to the sensitivity of combo moves (used to encourage more joystick movement & regular attacks)
local ComboMoves = require("ANN_sf2hf/ComboMoves")

combo_index_start = 11 -- Index of the neural network outputs where special moves start
combo_index_end = 79 -- Index of the neural network outputs where special moves end






--
-- Genetic Algorithm
--
local AI_GA = require("ANN_sf2hf/ANN_GA")
local GA = AI_GA:new()

local P1_fitness = {}	-- Fitness of each genome for player 1
local P1_genomes = {}	-- Genomes in the current generation for player 1
local P2_fitness = {}	-- Fitness of each genome for player 2
local P2_genomes = {}	-- Genomes in the current generation for player 2


local savefile = "plugins/ANN_sf2hf/genome.txt"  -- Output file path/name to save and load 

local P1_generation_wins = 0		-- Track wins/losses/draws per generation for player 1
local P1_generation_losses = 0
local P1_generation_draws = 0

local P2_generation_wins = 0		-- Track wins/losses/draws per generation for player 2
local P2_generation_losses = 0
local P2_generation_draws = 0

local current_generation = 1	-- Which generation of neural network are we currently processing
local current_genome = 1 		-- Which genome are we currently processing
local total_genomes = 100		-- Total number of genomes per generation
local num_fitparents = 10		-- The fittest N genomes will be chosen to breed the next generation
local num_randomparents = 2		-- N of the remaining genomes will be chosen at random to also breed the next generation
local mutationrate = 250		-- % of the genome that will be mutated (on average).  Note that this is in 100ths of a percent, so
								--  a mutationrate of 1 equals a .01% chance of mutation _per gene_.




-- Create a neural network for player 1
P1_NN = AI_NN:new()
-- Multiple hidden layers can be used...
P1_NN:AddLayer(70)	-- Input layer
P1_NN:AddLayer(55)	-- Hidden layer
P1_NN:AddLayer(40)	-- Hidden layer
P1_NN:AddLayer(79)	-- Output layer

-- Create a neural network for player 2
P2_NN = AI_NN:new()
-- Multiple hidden layers can be used...
P2_NN:AddLayer(70)	-- Input layer
P2_NN:AddLayer(55)	-- Hidden layer
P2_NN:AddLayer(40)	-- Hidden layer
P2_NN:AddLayer(79)	-- Output layer



--[[ Input Layer Chart:
 		Node		Description
		----		-----------
		1			Player 1 Health 
		2			Player 2 Health
		3			Player 1 Facing
		4-15		Player 1 Character (each node is "on" or "off" to indicate which character)
		16-27		Player 2 Character (each node is "on" or "off" to indicate which character)
		28-35		Player 1 State (standing, jumping, etc) (each node is "on" or "off" to indicate which state)
		36-43		Player 2 State (standing, jumping, etc) (each node is "on" or "off" to indicate which state)
		44			X Distance Between Characters
		45			Y Distance Between Characters			
		46			Game Timer
		47-70		Projectile Slots 0-7, Active, X Distance to Character, Y Distance to Character
--]]
		
--[[ Output Layer Chart:
 		Node		Description
		----		-----------
		1			Joystick Up
		2			Joystick Down
		3			Joystick Left
		4			Joystick Right
		5			Jab Punch
		6			Strong Punch
		7			Fierce Punch
		8			Short Kick
		9			Forward Kick
		10			Roundhouse Kick
		11-79		Special moves that require a combination of button presses
--]]





-- Setup each genome by generating random data in a neural network, then storing it in the genomes table
for i = 1, total_genomes do
	P1_NN:Prepare()		
	P1_genomes[i] = P1_NN:Serialize()
	P2_NN:Prepare()		
	P2_genomes[i] = P2_NN:Serialize()
end



--
-- Saving/Loading 
--

--
-- SaveGenomes() - Saves genomes to the specified file
--
function SaveGenomes(filename)
	file = io.open(filename, "w")
	io.output(file)

	-- Write generation info
	str = current_generation
	io.write(str .. "\n")

	-- Loop through each genome
	for i = 1, total_genomes do
		-- Write each genome to file, one value per line (player 1)
		for j = 1, #P1_genomes[i] do
			str = P1_genomes[i][j]
			io.write(str .. "\n")
		end
		-- Write each genome to file, one value per line (player 2)
		for j = 1, #P2_genomes[i] do
			str = P2_genomes[i][j]
			io.write(str .. "\n")
		end		
	end

	io.close(file)
end


--
-- LoadGenomes() - Loads genomes from the specified file. Note that if the structure of the neural
--  network or number of genomes has changed, you may encounter errors from an out-of-date
--  save.  Be sure to delete the old saved data before running again.
--
function LoadGenomes(filename)
	file = io.open(filename, "r")
	if file ~= nil then -- Only load if file exists
		io.input(file)

		-- Read generation info
		str = io.read()
		current_generation = tonumber(str)

		-- Loop through each genome
		for i = 1, total_genomes do
			-- Read each value from file (player 1)
			for j = 1, #P1_genomes[i] do
				str = io.read()
				P1_genomes[i][j] = tonumber(str)
			end
			-- Read each value from file (player 2)
			for j = 1, #P2_genomes[i] do
				str = io.read()
				P2_genomes[i][j] = tonumber(str)
			end			
		end

		io.close(file)
	end
end


--
-- FileExists() - Checks if the given file exists, returns true if the file exists
--
function FileExists(filename)
	file = io.open(filename, "r")
	if file ~= nil then 
		return true
	else
		return false
	end
end











--
-- AI
--


-- Keep track of the input buttons that the AI will use and their on/off state
AIButtons = {}
AIButtons["P1 Up"] = { button = buttons1["P1 Up"], pressed = false }
AIButtons["P1 Down"] = { button = buttons1["P1 Down"], pressed = false }
AIButtons["P1 Left"] = { button = buttons1["P1 Left"], pressed = false }
AIButtons["P1 Right"] = { button = buttons1["P1 Right"], pressed = false }
AIButtons["P1 Jab Punch"] = { button = buttons1["P1 Jab Punch"], pressed = false }
AIButtons["P1 Strong Punch"] = { button = buttons1["P1 Strong Punch"], pressed = false }
AIButtons["P1 Fierce Punch"] = { button = buttons1["P1 Fierce Punch"], pressed = false }
AIButtons["P1 Short Kick"] = { button = buttons2["P1 Short Kick"], pressed = false }
AIButtons["P1 Forward Kick"] = { button = buttons2["P1 Forward Kick"], pressed = false }
AIButtons["P1 Roundhouse Kick"] = { button = buttons2["P1 Roundhouse Kick"], pressed = false }

AIButtons["P2 Up"] = { button = buttons1["P2 Up"], pressed = false }
AIButtons["P2 Down"] = { button = buttons1["P2 Down"], pressed = false }
AIButtons["P2 Left"] = { button = buttons1["P2 Left"], pressed = false }
AIButtons["P2 Right"] = { button = buttons1["P2 Right"], pressed = false }
AIButtons["P2 Jab Punch"] = { button = buttons1["P2 Jab Punch"], pressed = false }
AIButtons["P2 Strong Punch"] = { button = buttons1["P2 Strong Punch"], pressed = false }
AIButtons["P2 Fierce Punch"] = { button = buttons1["P2 Fierce Punch"], pressed = false }
AIButtons["P2 Short Kick"] = { button = buttons2["P2 Short Kick"], pressed = false }
AIButtons["P2 Forward Kick"] = { button = buttons2["P2 Forward Kick"], pressed = false }
AIButtons["P2 Roundhouse Kick"] = { button = buttons2["P2 Roundhouse Kick"], pressed = false }



--
-- ProcessAI - Process the AI based on the current game state
--
function ProcessAI()
	-- Clear the state of each button
	AIButtons["P1 Up"].pressed = false
	AIButtons["P1 Down"].pressed = false
	AIButtons["P1 Left"].pressed = false
	AIButtons["P1 Right"].pressed = false
	AIButtons["P1 Jab Punch"].pressed = false
	AIButtons["P1 Strong Punch"].pressed = false
	AIButtons["P1 Fierce Punch"].pressed = false
	AIButtons["P1 Short Kick"].pressed = false
	AIButtons["P1 Forward Kick"].pressed = false
	AIButtons["P1 Roundhouse Kick"].pressed = false

	AIButtons["P2 Up"].pressed = false
	AIButtons["P2 Down"].pressed = false
	AIButtons["P2 Left"].pressed = false
	AIButtons["P2 Right"].pressed = false
	AIButtons["P2 Jab Punch"].pressed = false
	AIButtons["P2 Strong Punch"].pressed = false
	AIButtons["P2 Fierce Punch"].pressed = false
	AIButtons["P2 Short Kick"].pressed = false
	AIButtons["P2 Forward Kick"].pressed = false
	AIButtons["P2 Roundhouse Kick"].pressed = false


	if P1_Enabled() then
		-- Feed the neural network input data
		-- Some input data is intentionaly inflated to provide bias of importance for that input (ex. facing direction)
		P1_NN:SetInput(1,P1_GetHealth()) -- Range 0-144
		P1_NN:SetInput(2,P2_GetHealth()) -- Range 0-144
		
		if P1_GetPositionX() < P2_GetPositionX() then	-- Player 1 facing right
			P1_NN:SetInput(3,500)
		else -- Player 1 facing left
			P1_NN:SetInput(3,0)
		end

		-- Each character has a unique set of moves, hitboxes, and AI routines, therefore
		-- several input nodes will be created to help the network learn to handle them
		if P1_GetCharacter() == 0 then P1_NN:SetInput(4, 400) else P1_NN:SetInput(4, 0) end -- P1 is Ryu
		if P1_GetCharacter() == 1 then P1_NN:SetInput(5, 400) else P1_NN:SetInput(5, 0) end -- P1 is E Honda
		if P1_GetCharacter() == 2 then P1_NN:SetInput(6, 400) else P1_NN:SetInput(6, 0) end -- P1 is Blanka
		if P1_GetCharacter() == 3 then P1_NN:SetInput(7, 400) else P1_NN:SetInput(7, 0) end -- P1 is Guile
		if P1_GetCharacter() == 4 then P1_NN:SetInput(8, 400) else P1_NN:SetInput(8, 0) end -- P1 is Ken
		if P1_GetCharacter() == 5 then P1_NN:SetInput(9, 400) else P1_NN:SetInput(9, 0) end -- P1 is Chun Li
		if P1_GetCharacter() == 6 then P1_NN:SetInput(10, 400) else P1_NN:SetInput(10, 0) end -- P1 is Zangief
		if P1_GetCharacter() == 7 then P1_NN:SetInput(11, 400) else P1_NN:SetInput(11, 0) end -- P1 is Dhalsim
		if P1_GetCharacter() == 8 then P1_NN:SetInput(12, 400) else P1_NN:SetInput(12, 0) end -- P1 is M Bison
		if P1_GetCharacter() == 9 then P1_NN:SetInput(13, 400) else P1_NN:SetInput(13, 0) end -- P1 is Sagat
		if P1_GetCharacter() == 10 then P1_NN:SetInput(14, 400) else P1_NN:SetInput(14, 0) end -- P1 is Balrog
		if P1_GetCharacter() == 11 then P1_NN:SetInput(15, 400) else P1_NN:SetInput(15, 0) end -- P1 is Vega

		if P2_GetCharacter() == 0 then P1_NN:SetInput(16, 400) else P1_NN:SetInput(16, 0) end -- P2 is Ryu
		if P2_GetCharacter() == 1 then P1_NN:SetInput(17, 400) else P1_NN:SetInput(17, 0) end -- P2 is E Honda
		if P2_GetCharacter() == 2 then P1_NN:SetInput(18, 400) else P1_NN:SetInput(18, 0) end -- P2 is Blanka
		if P2_GetCharacter() == 3 then P1_NN:SetInput(19, 400) else P1_NN:SetInput(19, 0) end -- P2 is Guile
		if P2_GetCharacter() == 4 then P1_NN:SetInput(20, 400) else P1_NN:SetInput(20, 0) end -- P2 is Ken
		if P2_GetCharacter() == 5 then P1_NN:SetInput(21, 400) else P1_NN:SetInput(21, 0) end -- P2 is Chun Li
		if P2_GetCharacter() == 6 then P1_NN:SetInput(22, 400) else P1_NN:SetInput(22, 0) end -- P2 is Zangief
		if P2_GetCharacter() == 7 then P1_NN:SetInput(23, 400) else P1_NN:SetInput(23, 0) end -- P2 is Dhalsim
		if P2_GetCharacter() == 8 then P1_NN:SetInput(24, 400) else P1_NN:SetInput(24, 0) end -- P2 is M Bison
		if P2_GetCharacter() == 9 then P1_NN:SetInput(25, 400) else P1_NN:SetInput(25, 0) end -- P2 is Sagat
		if P2_GetCharacter() == 10 then P1_NN:SetInput(26, 400) else P1_NN:SetInput(26, 0) end -- P2 is Balrog
		if P2_GetCharacter() == 11 then P1_NN:SetInput(27, 400) else P1_NN:SetInput(27, 0) end -- P2 is Vega


		-- Similarly, there is no linear relationship between states, so several input nodes will be
		-- created to help the network learn how to handle various character states
		if P1_GetState() == 0 then P1_NN:SetInput(28, 200) else P1_NN:SetInput(28, 0) end -- P1 is standing
		if P1_GetState() == 2 then P1_NN:SetInput(29, 200) else P1_NN:SetInput(29, 0) end -- P1 is crouching
		if P1_GetState() == 4 then P1_NN:SetInput(30, 200) else P1_NN:SetInput(30, 0) end -- P1 is jumping
		if P1_GetState() == 6 then P1_NN:SetInput(31, 200) else P1_NN:SetInput(31, 0) end -- P1 is jump-crouching
		if P1_GetState() == 8 then P1_NN:SetInput(32, 200) else P1_NN:SetInput(32, 0) end -- P1 is blocking
		if P1_GetState() == 10 then P1_NN:SetInput(33, 200) else P1_NN:SetInput(33, 0) end -- P1 is attacking
		if P1_GetState() == 14 then P1_NN:SetInput(34, 200) else P1_NN:SetInput(34, 0) end -- P1 is stunned
		if P1_GetState() == 20 then P1_NN:SetInput(35, 200) else P1_NN:SetInput(35, 0) end -- P1 is on ground

		if P2_GetState() == 0 then P1_NN:SetInput(36, 200) else P1_NN:SetInput(36, 0) end -- P2 is standing
		if P2_GetState() == 2 then P1_NN:SetInput(37, 200) else P1_NN:SetInput(37, 0) end -- P2 is crouching
		if P2_GetState() == 4 then P1_NN:SetInput(38, 200) else P1_NN:SetInput(38, 0) end -- P2 is jumping
		if P2_GetState() == 6 then P1_NN:SetInput(39, 200) else P1_NN:SetInput(39, 0) end -- P2 is jump-crouching
		if P2_GetState() == 8 then P1_NN:SetInput(40, 200) else P1_NN:SetInput(40, 0) end -- P2 is blocking
		if P2_GetState() == 10 then P1_NN:SetInput(41, 200) else P1_NN:SetInput(41, 0) end -- P2 is attacking
		if P2_GetState() == 14 then P1_NN:SetInput(42, 200) else P1_NN:SetInput(42, 0) end -- P2 is stunned
		if P2_GetState() == 20 then P1_NN:SetInput(43, 200) else P1_NN:SetInput(43, 0) end -- P2 is on ground


		-- Distance between characters
		P1_NN:SetInput(44, math.abs(P1_GetPositionX() - P2_GetPositionX())) -- Range 0-384 (< screen width)
		P1_NN:SetInput(45, math.abs(P1_GetPositionY() - P2_GetPositionY())) -- Range 0-224 (< screen height)

		P1_NN:SetInput(46, 153 - GetTimer()) -- 153 "ticks" in a round

		for i = 1, 8 do -- 8 projectiles
			if ProjectileActive(i-1) then
				P1_NN:SetInput(((i-1) * 3) + 47, 500)	-- Projectile Active
				P1_NN:SetInput(((i-1) * 3) + 48, 384 - math.abs(P1_GetPositionX() - ProjectilePositionX(i-1)))	-- Projectile X Distance/Closeness			
				P1_NN:SetInput(((i-1) * 3) + 49, 224 - math.abs(P1_GetPositionY() - ProjectilePositionY(i-1)))	-- Projectile Y Distance/Closeness			
			else
				P1_NN:SetInput(((i-1) * 3) + 47, 0)	-- Projectile Inactive
				P1_NN:SetInput(((i-1) * 3) + 48, 0) -- Projectile X Distance
				P1_NN:SetInput(((i-1) * 3) + 49, 0) -- Projectile Y Distance
			end
		end


		P1_NN:Update()
		

		-- Get the output states, compare to sensitivity
		-- Special combo moves override any other button presses, so check for those first
		combo_move_index = ComboMoves:CheckComboMoves(1)
		if combo_move_index ~= -1 then
			ComboMoves:ProcessComboMoves(combo_move_index,1)
		else
			if P1_NN:GetOutput(1) > sensitivity then
				AIButtons["P1 Up"].pressed = true
			end
			if P1_NN:GetOutput(2) > sensitivity then
				AIButtons["P1 Down"].pressed = true
			end
			if P1_NN:GetOutput(3) > sensitivity then
				AIButtons["P1 Left"].pressed = true
			end
			if P1_NN:GetOutput(4) > sensitivity then
				AIButtons["P1 Right"].pressed = true
			end
			if P1_NN:GetOutput(5) > sensitivity then
				AIButtons["P1 Jab Punch"].pressed = true
			end
			if P1_NN:GetOutput(6) > sensitivity then
				AIButtons["P1 Strong Punch"].pressed = true
			end
			if P1_NN:GetOutput(7) > sensitivity then
				AIButtons["P1 Fierce Punch"].pressed = true
			end
			if P1_NN:GetOutput(8) > sensitivity then
				AIButtons["P1 Short Kick"].pressed = true
			end
			if P1_NN:GetOutput(9) > sensitivity then
				AIButtons["P1 Forward Kick"].pressed = true
			end
			if P1_NN:GetOutput(10) > sensitivity then
				AIButtons["P1 Roundhouse Kick"].pressed = true
			end
			for i = combo_index_start, combo_index_end do -- Check for special moves
				if P1_NN:GetOutput(i) > (sensitivity + combo_move_bias) then 
					ComboMoves:StartMove(1,i)
				end
			end
		end
	end -- P1_Enabled




	if P2_Enabled() then
		-- Feed the neural network input data
		-- Some input data is intentionaly inflated to provide bias of importance for that input (ex. facing direction)
		P2_NN:SetInput(1,P1_GetHealth()) -- Range 0-144
		P2_NN:SetInput(2,P2_GetHealth()) -- Range 0-144

		if P1_GetPositionX() < P2_GetPositionX() then	-- Player 1 facing right
			P2_NN:SetInput(3,500)
		else -- Player 1 facing left
			P2_NN:SetInput(3,0)
		end

		-- Each character has a unique set of moves, hitboxes, and AI routines, therefore
		-- several input nodes will be created to help the network learn to handle them
		if P1_GetCharacter() == 0 then P2_NN:SetInput(4, 400) else P2_NN:SetInput(4, 0) end -- P1 is Ryu
		if P1_GetCharacter() == 1 then P2_NN:SetInput(5, 400) else P2_NN:SetInput(5, 0) end -- P1 is E Honda
		if P1_GetCharacter() == 2 then P2_NN:SetInput(6, 400) else P2_NN:SetInput(6, 0) end -- P1 is Blanka
		if P1_GetCharacter() == 3 then P2_NN:SetInput(7, 400) else P2_NN:SetInput(7, 0) end -- P1 is Guile
		if P1_GetCharacter() == 4 then P2_NN:SetInput(8, 400) else P2_NN:SetInput(8, 0) end -- P1 is Ken
		if P1_GetCharacter() == 5 then P2_NN:SetInput(9, 400) else P2_NN:SetInput(9, 0) end -- P1 is Chun Li
		if P1_GetCharacter() == 6 then P2_NN:SetInput(10, 400) else P2_NN:SetInput(10, 0) end -- P1 is Zangief
		if P1_GetCharacter() == 7 then P2_NN:SetInput(11, 400) else P2_NN:SetInput(11, 0) end -- P1 is Dhalsim
		if P1_GetCharacter() == 8 then P2_NN:SetInput(12, 400) else P2_NN:SetInput(12, 0) end -- P1 is M Bison
		if P1_GetCharacter() == 9 then P2_NN:SetInput(13, 400) else P2_NN:SetInput(13, 0) end -- P1 is Sagat
		if P1_GetCharacter() == 10 then P2_NN:SetInput(14, 400) else P2_NN:SetInput(14, 0) end -- P1 is Balrog
		if P1_GetCharacter() == 11 then P2_NN:SetInput(15, 400) else P2_NN:SetInput(15, 0) end -- P1 is Vega

		if P2_GetCharacter() == 0 then P2_NN:SetInput(16, 400) else P2_NN:SetInput(16, 0) end -- P2 is Ryu
		if P2_GetCharacter() == 1 then P2_NN:SetInput(17, 400) else P2_NN:SetInput(17, 0) end -- P2 is E Honda
		if P2_GetCharacter() == 2 then P2_NN:SetInput(18, 400) else P2_NN:SetInput(18, 0) end -- P2 is Blanka
		if P2_GetCharacter() == 3 then P2_NN:SetInput(19, 400) else P2_NN:SetInput(19, 0) end -- P2 is Guile
		if P2_GetCharacter() == 4 then P2_NN:SetInput(20, 400) else P2_NN:SetInput(20, 0) end -- P2 is Ken
		if P2_GetCharacter() == 5 then P2_NN:SetInput(21, 400) else P2_NN:SetInput(21, 0) end -- P2 is Chun Li
		if P2_GetCharacter() == 6 then P2_NN:SetInput(22, 400) else P2_NN:SetInput(22, 0) end -- P2 is Zangief
		if P2_GetCharacter() == 7 then P2_NN:SetInput(23, 400) else P2_NN:SetInput(23, 0) end -- P2 is Dhalsim
		if P2_GetCharacter() == 8 then P2_NN:SetInput(24, 400) else P2_NN:SetInput(24, 0) end -- P2 is M Bison
		if P2_GetCharacter() == 9 then P2_NN:SetInput(25, 400) else P2_NN:SetInput(25, 0) end -- P2 is Sagat
		if P2_GetCharacter() == 10 then P2_NN:SetInput(26, 400) else P2_NN:SetInput(26, 0) end -- P2 is Balrog
		if P2_GetCharacter() == 11 then P2_NN:SetInput(27, 400) else P2_NN:SetInput(27, 0) end -- P2 is Vega

		-- Similarly, there is no linear relationship between states, so several input nodes will be
		-- created to help the network learn how to handle various character states
		if P1_GetState() == 0 then P2_NN:SetInput(28, 200) else P2_NN:SetInput(28, 0) end -- P1 is standing
		if P1_GetState() == 2 then P2_NN:SetInput(29, 200) else P2_NN:SetInput(29, 0) end -- P1 is crouching
		if P1_GetState() == 4 then P2_NN:SetInput(30, 200) else P2_NN:SetInput(30, 0) end -- P1 is jumping
		if P1_GetState() == 6 then P2_NN:SetInput(31, 200) else P2_NN:SetInput(31, 0) end -- P1 is jump-crouching
		if P1_GetState() == 8 then P2_NN:SetInput(32, 200) else P2_NN:SetInput(32, 0) end -- P1 is blocking
		if P1_GetState() == 10 then P2_NN:SetInput(33, 200) else P2_NN:SetInput(33, 0) end -- P1 is attacking
		if P1_GetState() == 14 then P2_NN:SetInput(34, 200) else P2_NN:SetInput(34, 0) end -- P1 is stunned
		if P1_GetState() == 20 then P2_NN:SetInput(35, 200) else P2_NN:SetInput(35, 0) end -- P1 is on ground

		if P2_GetState() == 0 then P2_NN:SetInput(36, 200) else P2_NN:SetInput(36, 0) end -- P2 is standing
		if P2_GetState() == 2 then P2_NN:SetInput(37, 200) else P2_NN:SetInput(37, 0) end -- P2 is crouching
		if P2_GetState() == 4 then P2_NN:SetInput(38, 200) else P2_NN:SetInput(38, 0) end -- P2 is jumping
		if P2_GetState() == 6 then P2_NN:SetInput(39, 200) else P2_NN:SetInput(39, 0) end -- P2 is jump-crouching
		if P2_GetState() == 8 then P2_NN:SetInput(40, 200) else P2_NN:SetInput(40, 0) end -- P2 is blocking
		if P2_GetState() == 10 then P2_NN:SetInput(41, 200) else P2_NN:SetInput(41, 0) end -- P2 is attacking
		if P2_GetState() == 14 then P2_NN:SetInput(42, 200) else P2_NN:SetInput(42, 0) end -- P2 is stunned
		if P2_GetState() == 20 then P2_NN:SetInput(43, 200) else P2_NN:SetInput(43, 0) end -- P2 is on ground


		-- Distance between characters
		P2_NN:SetInput(44, math.abs(P1_GetPositionX() - P2_GetPositionX())) -- Range 0-384 (< screen width)
		P2_NN:SetInput(45, math.abs(P1_GetPositionY() - P2_GetPositionY())) -- Range 0-224 (< screen height)

		P2_NN:SetInput(46, 153 - GetTimer()) -- 153 "ticks" in a round


		for i = 1, 8 do -- 8 projectiles
			if ProjectileActive(i-1) then
				P2_NN:SetInput(((i-1) * 3) + 47, 500)	-- Projectile Active
				P2_NN:SetInput(((i-1) * 3) + 48, 384 - math.abs(P2_GetPositionX() - ProjectilePositionX(i-1)))	-- Projectile X Distance/Closeness			
				P2_NN:SetInput(((i-1) * 3) + 49, 224 - math.abs(P2_GetPositionY() - ProjectilePositionY(i-1)))	-- Projectile Y Distance/Closeness			
			else
				P2_NN:SetInput(((i-1) * 3) + 47, 0)	-- Projectile Inactive
				P2_NN:SetInput(((i-1) * 3) + 48, 0) -- Projectile X Distance
				P2_NN:SetInput(((i-1) * 3) + 49, 0) -- Projectile Y Distance
			end
		end



		P2_NN:Update()
		

		-- Get the output states, compare to sensitivity
		-- Special combo moves override any other button presses, so check for those first
		combo_move_index = ComboMoves:CheckComboMoves(2)
		if combo_move_index ~= -1 then
			ComboMoves:ProcessComboMoves(combo_move_index,2)
		else
			if P2_NN:GetOutput(1) > sensitivity then
				AIButtons["P2 Up"].pressed = true
			end
			if P2_NN:GetOutput(2) > sensitivity then
				AIButtons["P2 Down"].pressed = true
			end
			if P2_NN:GetOutput(3) > sensitivity then
				AIButtons["P2 Left"].pressed = true
			end
			if P2_NN:GetOutput(4) > sensitivity then
				AIButtons["P2 Right"].pressed = true
			end
			if P2_NN:GetOutput(5) > sensitivity then
				AIButtons["P2 Jab Punch"].pressed = true
			end
			if P2_NN:GetOutput(6) > sensitivity then
				AIButtons["P2 Strong Punch"].pressed = true
			end
			if P2_NN:GetOutput(7) > sensitivity then
				AIButtons["P2 Fierce Punch"].pressed = true
			end
			if P2_NN:GetOutput(8) > sensitivity then
				AIButtons["P2 Short Kick"].pressed = true
			end
			if P2_NN:GetOutput(9) > sensitivity then
				AIButtons["P2 Forward Kick"].pressed = true
			end
			if P2_NN:GetOutput(10) > sensitivity then
				AIButtons["P2 Roundhouse Kick"].pressed = true
			end
			for i = combo_index_start, combo_index_end do -- Check for special moves
				if P2_NN:GetOutput(i) > (sensitivity + combo_move_bias) then 
					ComboMoves:StartMove(2,i)
				end
			end
		end
	end -- P2_Enabled






	-- Check the state of each button and send the result to MAME
	-- Joystick
	if AIButtons["P1 Up"].pressed then
		AIButtons["P1 Up"].button:set_value(1)
	else
		AIButtons["P1 Up"].button:set_value(0)
	end
	if AIButtons["P1 Down"].pressed then
		AIButtons["P1 Down"].button:set_value(1)
	else
		AIButtons["P1 Down"].button:set_value(0)
	end
	if AIButtons["P1 Left"].pressed then
		AIButtons["P1 Left"].button:set_value(1)
	else
		AIButtons["P1 Left"].button:set_value(0)
	end
	if AIButtons["P1 Right"].pressed then
		AIButtons["P1 Right"].button:set_value(1)
	else
		AIButtons["P1 Right"].button:set_value(0)
	end

	if AIButtons["P2 Up"].pressed then
		AIButtons["P2 Up"].button:set_value(1)
	else
		AIButtons["P2 Up"].button:set_value(0)
	end
	if AIButtons["P2 Down"].pressed then
		AIButtons["P2 Down"].button:set_value(1)
	else
		AIButtons["P2 Down"].button:set_value(0)
	end
	if AIButtons["P2 Left"].pressed then
		AIButtons["P2 Left"].button:set_value(1)
	else
		AIButtons["P2 Left"].button:set_value(0)
	end
	if AIButtons["P2 Right"].pressed then
		AIButtons["P2 Right"].button:set_value(1)
	else
		AIButtons["P2 Right"].button:set_value(0)
	end

	-- Punches
	if AIButtons["P1 Jab Punch"].pressed then
		AIButtons["P1 Jab Punch"].button:set_value(1)
	else
		AIButtons["P1 Jab Punch"].button:set_value(0)
	end
	if AIButtons["P1 Strong Punch"].pressed then
		AIButtons["P1 Strong Punch"].button:set_value(1)
	else
		AIButtons["P1 Strong Punch"].button:set_value(0)
	end
	if AIButtons["P1 Fierce Punch"].pressed then
		AIButtons["P1 Fierce Punch"].button:set_value(1)
	else
		AIButtons["P1 Fierce Punch"].button:set_value(0)
	end

	if AIButtons["P2 Jab Punch"].pressed then
		AIButtons["P2 Jab Punch"].button:set_value(1)
	else
		AIButtons["P2 Jab Punch"].button:set_value(0)
	end
	if AIButtons["P2 Strong Punch"].pressed then
		AIButtons["P2 Strong Punch"].button:set_value(1)
	else
		AIButtons["P2 Strong Punch"].button:set_value(0)
	end
	if AIButtons["P2 Fierce Punch"].pressed then
		AIButtons["P2 Fierce Punch"].button:set_value(1)
	else
		AIButtons["P2 Fierce Punch"].button:set_value(0)
	end


	-- Kicks
	if AIButtons["P1 Short Kick"].pressed then
		AIButtons["P1 Short Kick"].button:set_value(1)
	else
		AIButtons["P1 Short Kick"].button:set_value(0)
	end
	if AIButtons["P1 Forward Kick"].pressed then
		AIButtons["P1 Forward Kick"].button:set_value(1)
	else
		AIButtons["P1 Forward Kick"].button:set_value(0)
	end
	if AIButtons["P1 Roundhouse Kick"].pressed then
		AIButtons["P1 Roundhouse Kick"].button:set_value(1)
	else
		AIButtons["P1 Roundhouse Kick"].button:set_value(0)
	end

	if AIButtons["P2 Short Kick"].pressed then
		AIButtons["P2 Short Kick"].button:set_value(1)
	else
		AIButtons["P2 Short Kick"].button:set_value(0)
	end
	if AIButtons["P2 Forward Kick"].pressed then
		AIButtons["P2 Forward Kick"].button:set_value(1)
	else
		AIButtons["P2 Forward Kick"].button:set_value(0)
	end
	if AIButtons["P2 Roundhouse Kick"].pressed then
		AIButtons["P2 Roundhouse Kick"].button:set_value(1)
	else
		AIButtons["P2 Roundhouse Kick"].button:set_value(0)
	end


	-- Check for the end of the round
	if GetRoundDone() then
		if not endround_processed then	-- Be sure to process the end of the round only once
			x = GetRoundWinner()
			if x == 1 then -- Player 1 Wins Round
				--[[ Alternate fitness calculations...
				-- Increase fitness of current player 1 genome for winning
				P1_fitness[current_genome] = P1_fitness[current_genome] + 500

				if P1_GetHealth() == 144 then
					-- Large extra bonus for a perfect round
					P1_fitness[current_genome] = P1_fitness[current_genome] + 250
				else
					-- Decrease fitness for damage taken
					health = P1_GetHealth()
					if health == 65535 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
						health = 0
					end
					P1_fitness[current_genome] = P1_fitness[current_genome] - (2 * (144 - health))
				end
				


				-- Increase fitness of current player 2 genome for damage done 
				health = P1_GetHealth()
				if health == 65535 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
					health = 0
				end
				P2_fitness[current_genome] = P2_fitness[current_genome] + (144 - health)

				-- Increase fitness of current player 2 genome for time survived
				P2_fitness[current_genome] = P2_fitness[current_genome] + (153 - GetTimer()) -- 153 "ticks" in a round (hex 99 display "seconds")				
				--]]


				P1Wins = P1Wins + 1
				P1_generation_wins = P1_generation_wins + 1
				P2_generation_losses = P2_generation_losses + 1
			elseif x == 2 then -- Player 2 Wins Round
				--[[ Alternate fitness calculations...
				-- Increase fitness of current player 1 genome for damage done 
				health = P2_GetHealth()
				if health == 65535 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
					health = 0
				end
				P1_fitness[current_genome] = P1_fitness[current_genome] + (144 - health)

				-- Increase fitness of current player 1 genome for time survived
				P1_fitness[current_genome] = P1_fitness[current_genome] + (153 - GetTimer()) -- 153 "ticks" in a round (hex 99 display "seconds")				



				-- Increase fitness of current player 2 genome for winning
				P2_fitness[current_genome] = P2_fitness[current_genome] + 500

				if P2_GetHealth() == 144 then
					-- Large extra bonus for a perfect round
					P2_fitness[current_genome] = P2_fitness[current_genome] + 250
				else
					-- Decrease fitness for damage taken
					health = P2_GetHealth()
					if health == 65535 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
						health = 0
					end
					P2_fitness[current_genome] = P2_fitness[current_genome] - (2 * (144 - health))
				end
				--]]

				P2Wins = P2Wins + 1
				P2_generation_wins = P2_generation_wins + 1				
				P1_generation_losses = P1_generation_losses + 1
			elseif x == 255 then -- Round Draw
				--[[ Alternate fitness calculations...
				-- Increase P1 fitness for damage done 
				health = P2_GetHealth()
				if health == 65535 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
					health = 0
				end
				P1_fitness[current_genome] = P1_fitness[current_genome] + (144 - health)

				-- Increase P2 fitness for damage done 
				health = P1_GetHealth()
				if health == 65535 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
					health = 0
				end
				P2_fitness[current_genome] = P2_fitness[current_genome] + (144 - health)
				--]]

				
				Draws = Draws + 1
				P1_generation_draws = P1_generation_draws + 1
				P2_generation_draws = P2_generation_draws + 1
			else
				print("Unknown end of match value: " .. x) 
			end


			-- Increase P1 fitness for damage done 
			health = P2_GetHealth()
			if health > 144 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
				health = 0
			end
			P1_fitness[current_genome] = P1_fitness[current_genome] + (3 * (144 - health))

			-- Decrease P2 fitness for damage taken
			P2_fitness[current_genome] = P2_fitness[current_genome] - (144 - health)


			-- Increase P2 fitness for damage done 
			health = P1_GetHealth()
			if health > 144 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
				health = 0
			end
			P2_fitness[current_genome] = P2_fitness[current_genome] + (3 * (144 - health))

			-- Decrease P1 fitness for damage taken
			P1_fitness[current_genome] = P1_fitness[current_genome] - (144 - health)




			print("Generation " .. current_generation .. " Genome " .. current_genome .. " P1 Fitness: " .. P1_fitness[current_genome] .. ", P2 Fitness: " .. P2_fitness[current_genome])

			if P1Wins == 2 or P2Wins == 2 or Draws == 4 then -- Check for end of match (also end if long stalemate)		
				-- Increment the current genome
				current_genome = current_genome + 1

				-- If this was the last genome to be processed, start a new generation
				if current_genome > total_genomes then
					-- Player 1
					-- Store fitness and genome data in our genetic algorithm
					GA:StoreFitness(P1_fitness)
					GA:StoreGenomes(P1_genomes)

					-- Print some statistics
					total_fitness = 0
					for i = 1, #P1_fitness do
						total_fitness = total_fitness + P1_fitness[i]
					end
					print("P1 Generation " .. current_generation .. " W/L/D: " .. P1_generation_wins .. "/" .. P1_generation_losses .. "/" .. P1_generation_draws)
					print("P1 Average fitness: " .. total_fitness / total_genomes)


					-- Breed a new generation
					P1_genomes = GA:Breed(num_fitparents, num_randomparents, mutationrate)
					P1_fitness = {}


					-- Player 2
					-- Store fitness and genome data in our genetic algorithm
					GA:StoreFitness(P2_fitness)
					GA:StoreGenomes(P2_genomes)

					-- Print some statistics
					total_fitness = 0
					for i = 1, #P2_fitness do
						total_fitness = total_fitness + P2_fitness[i]
					end
					print("P2 Generation " .. current_generation .. " W/L/D: " .. P2_generation_wins .. "/" .. P2_generation_losses .. "/" .. P2_generation_draws)
					print("P2 Average fitness: " .. total_fitness / total_genomes)


					-- Breed a new generation
					P2_genomes = GA:Breed(num_fitparents, num_randomparents, mutationrate)
					P2_fitness = {}


					current_generation = current_generation + 1
					current_genome = 1
					P1_generation_wins = 0
					P1_generation_losses = 0
					P1_generation_draws = 0
					P2_generation_wins = 0
					P2_generation_losses = 0
					P2_generation_draws = 0

					-- Save at the beginning of each generation
					SaveGenomes(savefile)
				end

				-- Load the next genomes into the neural networks
				P1_NN:Deserialize(P1_genomes[current_genome])
				P1_fitness[current_genome] = 0
				P2_NN:Deserialize(P2_genomes[current_genome])
				P2_fitness[current_genome] = 0

				P1Wins = 0
				P2Wins = 0
				Draws = 0
			end


			endround_processed = true
		end
	else
		endround_processed = false
	end
end





--
-- MAME Operation
--

--
-- main() - Callback for when MAME has finished processing a frame of the emulated machine
--
function main()
	if game_started then
		if frame_num % frame_throttle == 0 then	-- Throttle AI processing
			ProcessAI()
		end

		if continue_game then
			-- Insert a coin for Player 1
			if frame_continue == 0 then
				buttons0["Coin 1"]:set_value(1)
			end
			if frame_continue == 2 then
				buttons0["Coin 1"]:set_value(0)
			end

			-- Press the start button for Player 1
			if frame_continue == 3 then
				buttons0["1 Player Start"]:set_value(1)
			end
			if frame_continue == 4 then
				buttons0["1 Player Start"]:set_value(0)
			end

			-- Insert a coin for Player 2
			if frame_continue == 6 then
				buttons0["Coin 2"]:set_value(1)
			end
			if frame_continue == 8 then
				buttons0["Coin 2"]:set_value(0)
			end

			-- Press the start button for Player 2
			if frame_continue == 9 then
				buttons0["2 Players Start"]:set_value(1)
			end
			if frame_continue == 10 then
				buttons0["2 Players Start"]:set_value(0)
				continue_game = false
				select_character = true
				frame_select = 0
				ClearContinueTimes() -- Clear out the continue time for both players so this sequence only fires once per continue
			end

			frame_continue = frame_continue + 1
		elseif select_character then
			-- Select a character at random.  The winner of the previous match will be locked in
			-- to their character, so we can safely run a random pick on both players without side effects
			-- Player 1
			if frame_select == 300 then -- Delay until character select screen displays
				select_rnd1 = math.random(0,5)
				select_rnd2 = math.random(0,1)
			elseif frame_select >= 302 and frame_select <= 311 then -- Move stick right
				if select_rnd1 > 0 then
					if frame_select % 2 == 0 then
						buttons1["P1 Right"]:set_value(1)
					else 
						buttons1["P1 Right"]:set_value(0)
						select_rnd1 = select_rnd1 - 1
					end
				end
			elseif frame_select >= 312 and frame_select <= 313 then -- Move stick up/down
				if select_rnd2 == 0 then 
					if frame_select % 2 == 0 then
						buttons1["P1 Up"]:set_value(1)
					else 
						buttons1["P1 Up"]:set_value(0)
					end
				elseif select_rnd2 == 1 then
					if frame_select % 2 == 0 then
						buttons1["P1 Down"]:set_value(1)
					else 
						buttons1["P1 Down"]:set_value(0)
					end
				end
			elseif frame_select == 314 then -- Select character with start or punch (different attires)
				select_rnd2 = math.random(0,1)
				if select_rnd2 == 0 then
					buttons0["1 Player Start"]:set_value(1)
				else
					buttons1["P1 Fierce Punch"]:set_value(1)
				end
			elseif frame_select == 315 then
				if select_rnd2 == 0 then
					buttons0["1 Player Start"]:set_value(0)
				else
					buttons1["P1 Fierce Punch"]:set_value(0)
				end

			end




			-- Player 2
			if frame_select == 316 then
				select_rnd1 = math.random(0,5)
				select_rnd2 = math.random(0,1)
			elseif frame_select >= 318 and frame_select <= 327 then -- Move stick right
				if select_rnd1 > 0 then
					if frame_select % 2 == 0 then
						buttons1["P2 Right"]:set_value(1)
					else 
						buttons1["P2 Right"]:set_value(0)
						select_rnd1 = select_rnd1 - 1
					end
				end
			elseif frame_select >= 328 and frame_select <= 329 then -- Move stick up/down
				if select_rnd2 == 0 then 
					if frame_select % 2 == 0 then
						buttons1["P2 Up"]:set_value(1)
					else 
						buttons1["P2 Up"]:set_value(0)
					end
				elseif select_rnd2 == 1 then
					if frame_select % 2 == 0 then
						buttons1["P2 Down"]:set_value(1)
					else 
						buttons1["P2 Down"]:set_value(0)
					end
				end
			elseif frame_select == 330 then -- Select character with start or punch (different attires)
				select_rnd2 = math.random(0,1)
				if select_rnd2 == 0 then
					buttons0["2 Players Start"]:set_value(1)
				else
					buttons1["P2 Fierce Punch"]:set_value(1)
				end
			elseif frame_select == 331 then
				if select_rnd2 == 0 then
					buttons0["2 Players Start"]:set_value(0)
				else
					buttons1["P2 Fierce Punch"]:set_value(0)
				end
				select_character = false
			end

			frame_select = frame_select + 1
		else -- not already in the process of continuing
			-- Check for continue screen
			if P1_GetContinueTime() > 0 or P2_GetContinueTime() > 0 then
				-- Insert a coin
				continue_game = true -- Set a flag that we are in the process of continuing a game
				frame_continue = 0 -- Keep track of the number of frames elapsed after starting our process of continuing
			end
		end

	else -- if NOT game_started
		-- Insert one coin when the title screen appears
		-- 1024 frames is more than enough to skip the loading screens
		-- Note, the coin button press appears to require two frames to detect!
		if (frame_num - frame_randomstart) == 1024 then
			buttons0["Coin 1"]:set_value(1)
			buttons0["Coin 2"]:set_value(1)
		end
		if (frame_num - frame_randomstart) == 1026 then
			buttons0["Coin 1"]:set_value(0)
			buttons0["Coin 2"]:set_value(0)
		end

		-- Start both players
		if (frame_num - frame_randomstart) == 1150 then
			buttons0["1 Player Start"]:set_value(1)
			buttons0["2 Players Start"]:set_value(1)
		end
		if (frame_num - frame_randomstart) == 1151 then
			buttons0["1 Player Start"]:set_value(0)
			buttons0["2 Players Start"]:set_value(0)
		end
	
		-- Select from character select screen
		if (frame_num - frame_randomstart) == 1250 then
			buttons0["1 Player Start"]:set_value(1)
			buttons0["2 Players Start"]:set_value(1)
		end
		if (frame_num - frame_randomstart) == 1251 then
			buttons0["1 Player Start"]:set_value(0)
			buttons0["2 Players Start"]:set_value(0)
			game_started = true
		end
		
	end -- if NOT game_started


	frame_num = frame_num + 1	
end


--
-- draw_hud() - Callback for when MAME has finished drawing a frame of the emulated machine
--
function draw_hud()
	-- All colors in ARGB format
	local color_green = 0xFF00FF00
	local color_red = 0xFFFF0000
	local color_blue = 0xFF0000FF
	local color_cyan = 0xFF00FFFF
	local color_black = 0xFF000000
	local color_gray25 = 0x40000000 -- 25% transparency
	local color_clear = 0x00000000
	local str = ""
	local x = 0


	-- Draw some translucent boxes as background
	manager:machine().screens[":screen"]:draw_box(0, 0, 384, 16, color_gray25, color_gray25)
	manager:machine().screens[":screen"]:draw_box(0, 190, 384, 224, color_gray25, color_gray25)

--[[]
	if P1_Enabled() then
		manager:machine().screens[":screen"]:draw_text(0, 0, "P1 Enabled", color_green)
	else
		manager:machine().screens[":screen"]:draw_text(0, 0, "P1 Disabled", color_green)
	end

	if P2_Enabled() then
		manager:machine().screens[":screen"]:draw_text(0, 8, "P2 Enabled", color_green)
	else
		manager:machine().screens[":screen"]:draw_text(0, 8, "P2 Disabled", color_green)
	end
--]]

	str = "Generation: " .. current_generation .. "  Genome: " .. current_genome
	manager:machine().screens[":screen"]:draw_text(0, 0, str, color_cyan)

	-- Calculate an average P1 fitness for this generation, only including completed genomes
	avg = 0
	if current_genome > 1 then
		for i = 1, current_genome - 1 do
			avg = avg + P1_fitness[i]
		end
		avg = avg / (current_genome - 1)
	end

	str = "P1 Avg. Fitness: " .. string.format("%.2f", avg) 
	manager:machine().screens[":screen"]:draw_text(0, 8, str, color_cyan)

	str = "P1 W/L/D: " .. P1_generation_wins .. "/" .. P1_generation_losses .. "/" .. P1_generation_draws
	manager:machine().screens[":screen"]:draw_text(80, 8, str, color_cyan)


	-- Calculate an average P2 fitness for this generation, only including completed genomes
	avg = 0
	if current_genome > 1 then
		for i = 1, current_genome - 1 do
			avg = avg + P2_fitness[i]
		end
		avg = avg / (current_genome - 1)
	end

	str = "P2 Avg. Fitness: " .. string.format("%.2f", avg) 
	manager:machine().screens[":screen"]:draw_text(240, 8, str, color_cyan)

	str = "P2 W/L/D: " .. P2_generation_wins .. "/" .. P2_generation_losses .. "/" .. P2_generation_draws
	manager:machine().screens[":screen"]:draw_text(320, 8, str, color_cyan)

	str = "P1 Health: " .. P1_GetHealth()
	manager:machine().screens[":screen"]:draw_text(118, 200, str, color_green)

	str = "P1 Pos: " .. P1_GetPositionX() .. ", " .. P1_GetPositionY()
	manager:machine().screens[":screen"]:draw_text(168, 200, str, color_green)

	str = "P1 State: " .. P1_GetState()
	manager:machine().screens[":screen"]:draw_text(228, 200, str, color_green)

	str = "P2 Health: " .. P2_GetHealth()
	manager:machine().screens[":screen"]:draw_text(118, 208, str, color_green)

	str = "P2 Pos: " .. P2_GetPositionX() .. ", " .. P2_GetPositionY()
	manager:machine().screens[":screen"]:draw_text(168, 208, str, color_green)

	str = "P2 State: " .. P2_GetState()
	manager:machine().screens[":screen"]:draw_text(228, 208, str, color_green)


	str = "Timer: " .. string.format("%x", GetTimer()) -- Format as hex for a friendly number of game "seconds"
	manager:machine().screens[":screen"]:draw_text(118, 216, str, color_green)

	if game_started then
		if GetRoundDone() then
			manager:machine().screens[":screen"]:draw_text(168, 216, "Round Over!", color_green)
			x = GetRoundWinner()
			if x == 1 then
				manager:machine().screens[":screen"]:draw_text(214, 216, "Player 1 Wins!", color_green)
			elseif x == 2 then
				manager:machine().screens[":screen"]:draw_text(214, 216, "Player 2 Wins!", color_green)				
			elseif x == 255 then
				manager:machine().screens[":screen"]:draw_text(214, 216, "Round Draw!", color_green)
			end
		else
			manager:machine().screens[":screen"]:draw_text(168, 216, "Round In Progress!", color_green)
		end
	else
			manager:machine().screens[":screen"]:draw_text(168, 216, "Game Not Started!", color_green)		
	end


	-- Projectile information display
	for i = 1, 8 do -- 8 projectiles
		if ProjectileActive(i-1) then
			str = "Projectile " .. i .. ": " .. ProjectilePositionX(i-1) .. ", " .. ProjectilePositionY(i-1)
			manager:machine().screens[":screen"]:draw_text(168, 136 + ((i-1) * 8), str, color_green)
		end
	end



	-- Check the state of each button and draw on screen
	-- Joystick
	if AIButtons["P1 Up"].pressed then
		manager:machine().screens[":screen"]:draw_box(22, 192, 38, 200, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(26, 192, "Up", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(22, 192, 38, 200, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(26, 192, "Up", color_blue)
	end
	if AIButtons["P1 Down"].pressed then
		manager:machine().screens[":screen"]:draw_box(22, 212, 38, 220, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(26, 212, "Dn", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(22, 212, 38, 220, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(26, 212, "Dn", color_blue)
	end
	if AIButtons["P1 Left"].pressed then
		manager:machine().screens[":screen"]:draw_box(4, 202, 20, 210, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(8, 202, "Lt", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(4, 202, 20, 210, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(8, 202, "Lt", color_blue)
	end
	if AIButtons["P1 Right"].pressed then
		manager:machine().screens[":screen"]:draw_box(40, 202, 56, 210, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(44, 202, "Rt", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(40, 202, 56, 210, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(44, 202, "Rt", color_blue)
	end

	-- Punches
	if AIButtons["P1 Jab Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(60, 198, 76, 206, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(64, 198, "JP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(60, 198, 76, 206, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(64, 198, "JP", color_blue)	
	end
	if AIButtons["P1 Strong Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(80, 198, 96, 206, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(84, 198, "SP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(80, 198, 96, 206, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(84, 198, "SP", color_blue)
	end
	if AIButtons["P1 Fierce Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(100, 198, 116, 206, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(104, 198, "FP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(100, 198, 116, 206, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(104, 198, "FP", color_blue)
	end

	-- Kicks
	if AIButtons["P1 Short Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(60, 210, 76, 218, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(64, 210, "SK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(60, 210, 76, 218, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(64, 210, "SK", color_blue)	
	end
	if AIButtons["P1 Forward Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(80, 210, 96, 218, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(84, 210, "FK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(80, 210, 96, 218, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(84, 210, "FK", color_blue)		
	end
	if AIButtons["P1 Roundhouse Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(100, 210, 116, 218, color_blue, color_black)
		manager:machine().screens[":screen"]:draw_text(104, 210, "RK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(100, 210, 116, 218, color_clear, color_blue)
		manager:machine().screens[":screen"]:draw_text(104, 210, "RK", color_blue)
	end	



	-- Joystick
	if AIButtons["P2 Up"].pressed then
		manager:machine().screens[":screen"]:draw_box(286, 192, 302, 200, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(290, 192, "Up", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(286, 192, 302, 200, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(290, 192, "Up", color_red)
	end
	if AIButtons["P2 Down"].pressed then
		manager:machine().screens[":screen"]:draw_box(286, 212, 302, 220, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(290, 212, "Dn", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(286, 212, 302, 220, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(290, 212, "Dn", color_red)
	end
	if AIButtons["P2 Left"].pressed then
		manager:machine().screens[":screen"]:draw_box(268, 202, 284, 210, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(272, 202, "Lt", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(268, 202, 284, 210, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(272, 202, "Lt", color_red)
	end
	if AIButtons["P2 Right"].pressed then
		manager:machine().screens[":screen"]:draw_box(304, 202, 320, 210, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(308, 202, "Rt", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(304, 202, 320, 210, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(308, 202, "Rt", color_red)
	end

	-- Punches
	if AIButtons["P2 Jab Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(324, 198, 340, 206, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(328, 198, "JP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(324, 198, 340, 206, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(328, 198, "JP", color_red)	
	end
	if AIButtons["P2 Strong Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(344, 198, 360, 206, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(348, 198, "SP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(344, 198, 360, 206, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(348, 198, "SP", color_red)
	end
	if AIButtons["P2 Fierce Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(364, 198, 380, 206, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(368, 198, "FP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(364, 198, 380, 206, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(368, 198, "FP", color_red)
	end

	-- Kicks
	if AIButtons["P2 Short Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(324, 210, 340, 218, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(328, 210, "SK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(324, 210, 340, 218, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(328, 210, "SK", color_red)	
	end
	if AIButtons["P2 Forward Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(344, 210, 360, 218, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(348, 210, "FK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(344, 210, 360, 218, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(348, 210, "FK", color_red)		
	end
	if AIButtons["P2 Roundhouse Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(364, 210, 380, 218, color_red, color_black)
		manager:machine().screens[":screen"]:draw_text(368, 210, "RK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(364, 210, 380, 218, color_clear, color_red)
		manager:machine().screens[":screen"]:draw_text(368, 210, "RK", color_red)
	end	


end




--
-- stop() - Callback for when MAME is stopped.  Note that this does NOT execute if you are using
--  "-video none" on the command line and CTRL-C to stop execution!
--
function stop()
	print("Frames Elapsed: " .. frame_num)

	-- Save our genomes to continue next time
	SaveGenomes(savefile)
end












--
-- Genetic Algorithm setup
--

-- If we have previously saved genome data, load it now 
if FileExists(savefile) then
	LoadGenomes(savefile)
end

-- Ready the first genomes
P1_NN:Deserialize(P1_genomes[current_genome])
P1_fitness[current_genome] = 0
P2_NN:Deserialize(P2_genomes[current_genome])
P2_fitness[current_genome] = 0







--
-- Mame Setup
--

-- Setup callbacks
emu.register_frame(main)
emu.register_frame_done(draw_hud, "frame")
emu.register_stop(stop)








