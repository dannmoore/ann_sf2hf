--[[
  File:    ANN_NN.lua
  Description: A simple neural network implementation
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
--]]


local AI_NN = {}
local Layer = {}
local Node = {}


--
-- randomFloat() - Generate a random floating point number
--  Note that this generates a number between [lower, upper), that is it will come close to but never exactly equal upper
--
function randomFloat(lower, upper)
	return lower + (math.random() * (upper - lower))
end



--
-- Node functions
--


--
-- Node:new() - Creeate a Node "object" and return it
--
function Node:new()
	local c = {weights = {}, value = 0} -- weights are to the NEXT layer
	setmetatable(c, self)
	self.__index = self	
	return c
end

--
-- Node:CreateWeights() - Creates and initializes the weights of this node
--
function Node:CreateWeights(numweights)
	for i = 1, numweights do
		self.weights[i] = 0
	end	
end


--
-- Node:RandomizeWeights() - Randomizes the weights of this node
--
function Node:RandomizeWeights()
	for i = 1, #self.weights do
		self.weights[i] = randomFloat(-1, 1)
	end	
end


--
-- Node:PrintDebugInfo() - Outputs the data of this node to the console
--
function Node:PrintDebugInfo() 
	str = "value = " .. self.value .. "  weights[] = "

	for i = 1, #self.weights do
		str = str .. self.weights[i]
		if i ~= #self.weights then
			str = str .. ", " -- Output a comma separator except on last element
		end 
	end

	print(str)
end








--
-- Layer functions
--

--
-- Layer:new() - Creeate a Layer "object" and return it
--
function Layer:new()
	local l = {nodes = {}}
	setmetatable(l, self)
	self.__index = self
	return l
end


--
-- Layer:RandomizeNodes() - Tells each node in this layer to randomize its values
--
function Layer:RandomizeNodes()
	for i = 1, #self.nodes do
		self.nodes[i]:RandomizeWeights()
	end
end


--
-- Layer:AddNodes() - Creates and adds the specified number of nodes to this layer
--
function Layer:AddNodes(num_nodes) 
	for i = 1, num_nodes do
		newnode = Node:new()
		self.nodes[#self.nodes + 1] = newnode
	end
end


--
-- Layer:CreateNodeWeights() - Tells each node in this layer to create the specified number of weight values
--
function Layer:CreateNodeWeights(numweights) 
	for i = 1, #self.nodes do
		self.nodes[i]:CreateWeights(numweights)
	end
end


--
-- Layer:GetWeightedNodeTotals() - Calculates the total value of all nodes in this layer, modified by weights, to the next node
--
function Layer:GetWeightedNodeTotals(nextnodeindex) 
	local total = 0

	for i = 1, #self.nodes do
		total = total + self.nodes[i].value * self.nodes[i].weights[nextnodeindex]
	end

	return total
end


--
-- Layer:GetNodeTotal() - Calculates the total value of nodes in this layer
--
function Layer:GetNodeTotal() 
	local total = 0

	for i = 1, #self.nodes do
		total = total + self.nodes[i].value
	end

	return total
end



--
-- Layer:ApplyActivation() - Applies a "squish" function to the given node in this layer
--
function Layer:ApplyActivation(index) 
	-- "squish" our node value using a sigmoid function to return values between -1.0 and 1.0
	-- We use a "fast sigmoid" here: f(x) = x / (1 + abs(x))	
	result = self.nodes[index].value / (1 + math.abs(self.nodes[index].value))
	self.nodes[index].value = result

--[[
	Alternate inverse square root function:
	-- "squish" our node value using an inverse square root function to return values between 0.0 and 1.0:
	-- f(x) = 1 / sqrt(1 + x^2)
	result = 1 / math.sqrt(1 + (self.nodes[index].GetValue() * self.nodes[index].GetValue()))
	self.nodes[index].value = result
--]]
end



--
-- Layer:AdjustWeights() - Adds the specified amount to the weights in the given node, useful for back propagation
--
function Layer:AdjustWeights(nodeindex, amount) 
	for i = 1, #self.nodes do
		newweight = self.nodes[i].weights[nodeindex] + amount
		-- Cap the new weight to our min/max values
		if newweight < -1.0 then 
			newweight = -1
		elseif newweight > 1 then 
			newweight = 1
		end
		self.nodes[i].weights[nodeindex] = newweight		
	end
end



--
-- Layer:PrintDebugInfo() - Outputs the data for each node in this layer to the console
--
function Layer:PrintDebugInfo() 
	for i = 1, #self.nodes do
		print("Node " .. i .. ": ")
		self.nodes[i]:PrintDebugInfo()
	end
end







--
-- Network functions
--

--
-- AI_NN:new() - Creeate a AI_NN "object" and return it
--
function AI_NN:new()
	local nn = {layers = {}}
	setmetatable(nn, self)
	self.__index = self
	return nn
end


--
-- AI_NN:AddLayer() - Adds a new layer with the number of specified nodes to the rightmode side of the network
--
function AI_NN:AddLayer(num_nodes) 
	newlayer = Layer:new()

	newlayer:AddNodes(num_nodes)
	self.layers[#self.layers + 1] = newlayer
end


--
-- AI_NN:Prepare() - Prepares the network for use by creating and randomizing node weight values
--
function AI_NN:Prepare() 
	for i = 1, #self.layers - 1 do
		self.layers[i]:CreateNodeWeights(#self.layers[i+1].nodes) -- pass in the number of nodes in the next layer to the right
		self.layers[i]:RandomizeNodes()
	end
end


--
-- AI_NN:SetInput() - Sets the given node of the input layer (always first layer) to the specified value
--
function AI_NN:SetInput(index, value) 
	self.layers[1].nodes[index].value = value
end


--
-- AI_NN:GetOutput() - Returns the value of the given node of the output layer (always last layer)
--
function AI_NN:GetOutput(index) 
	return self.layers[#self.layers].nodes[index].value 
end


--
-- AI_NN:Update() - Updates the neural network by forward propagation, to be called each time the input layer is changed
--
function AI_NN:Update() 
	-- for each node you take all of the previous node values * weight values, then add all those up.  Apply the activation function after that
	for i = 2, #self.layers do -- Start at the second layer
		for j = 1, #self.layers[i].nodes do
			nodetotal = self.layers[i - 1]:GetWeightedNodeTotals(j)
			self.layers[i].nodes[j].value = nodetotal
			self.layers[i]:ApplyActivation(j)  -- Apply activation function to node j, typically a sigmoid function
		end
	end
end


--
-- AI_NN:PrintDebugInfo() - Outputs the data for each layer to the console
--
function AI_NN:PrintDebugInfo() 
	print("Neural Network Debug Information:")

	for i = 1, #self.layers do
		print("Layer " .. i .. ":")
		self.layers[i]:PrintDebugInfo()
		print()
	end
end


--
-- AI_NN:Serialize() - Serializes the entire network into a single table of weight values
--
function AI_NN:Serialize()
	local result = {}
	local x = 1

	-- Loop through all layers and nodes, record the weights into a result table
	for i = 1, #self.layers - 1 do -- All except last layer 
		for j = 1, #self.layers[i].nodes do
			for k = 1, #self.layers[i].nodes[j].weights do
				result[x] = self.layers[i].nodes[j].weights[k]
				x = x + 1
			end
		end
	end

	return result
end


--
-- AI_NN:Deserialize() - Given a table of weights, deserialize back into their respecitve nodes
--
function AI_NN:Deserialize(data)
	local x = 1

	-- Loop through all layers and nodes, load the weights from the given data
	for i = 1, #self.layers - 1 do -- All except last layer 
		for j = 1, #self.layers[i].nodes do
			for k = 1, #self.layers[i].nodes[j].weights do
				self.layers[i].nodes[j].weights[k] = data[x]
				x = x + 1
			end
		end
	end
end



return AI_NN