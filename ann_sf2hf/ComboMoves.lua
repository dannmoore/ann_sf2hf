--[[
  File:    ComboMoves.lua
  Description: Helper functions to assist the AI in performing special combo moves
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
--]]



local ComboMoves = {}



-- To assist the AI, keep track of the current step in the sequence
local P1_combo_move_step = {}
local P2_combo_move_step = {}
-- Use the index that matches the neural network output layer for each special combo
-- Initialize to -1 to indicate the combo move is not currently being performed
--- Note about charge moves - Since the AI is throttled it doesnt process every frame, therefore
---  the steps are not 1:1 with frames, so experimentation is necessary to find the appropriate
---  charge time for each move if frame_throttle is changed.
P1_combo_move_step[11] = -1 -- Down, down-right, right, jab punch (fireball motion)
P1_combo_move_step[12] = -1 -- Right, down, down-right, jab punch (dragon punch motion)	
P1_combo_move_step[13] = -1 -- Down, down-left, left, short kick (hurricane kick motion)

P1_combo_move_step[14] = -1 -- Down, down-left, left, jab punch (fireball motion)
P1_combo_move_step[15] = -1 -- left, down, down-left, jab punch (dragon punch motion)	
P1_combo_move_step[16] = -1 -- Down, down-right, right, short kick (hurricane kick motion)

P1_combo_move_step[17] = -1 -- Down, down-right, right, strong punch (fireball motion)
P1_combo_move_step[18] = -1 -- Right, down, down-right, strong punch (dragon punch motion)	
P1_combo_move_step[19] = -1 -- Down, down-left, left, forward kick (hurricane kick motion)

P1_combo_move_step[20] = -1 -- Down, down-left, left, strong punch (fireball motion)
P1_combo_move_step[21] = -1 -- left, down, down-left, strong punch (dragon punch motion)	
P1_combo_move_step[22] = -1 -- Down, down-right, right, forward kick (hurricane kick motion)

P1_combo_move_step[23] = -1 -- Down, down-right, right, fierce punch (fireball motion)
P1_combo_move_step[24] = -1 -- Right, down, down-right, fierce punch (dragon punch motion)	
P1_combo_move_step[25] = -1 -- Down, down-left, left, roundhouse kick (hurricane kick motion)

P1_combo_move_step[26] = -1 -- Down, down-left, left, fierce punch (fireball motion)
P1_combo_move_step[27] = -1 -- left, down, down-left, fierce punch (dragon punch motion)	
P1_combo_move_step[28] = -1 -- Down, down-right, right, roundhouse kick (hurricane kick motion)

P1_combo_move_step[29] = -1 -- Charge left 2 seconds, right + jab punch (guile sonic boom, etc)
P1_combo_move_step[30] = -1 -- Charge left 2 seconds, right + strong punch (guile sonic boom, etc)
P1_combo_move_step[31] = -1 -- Charge left 2 seconds, right + fierce punch (guile sonic boom, etc)

P1_combo_move_step[32] = -1 -- Charge right 2 seconds, left + jab punch (guile sonic boom, etc)
P1_combo_move_step[33] = -1 -- Charge right 2 seconds, left + strong punch (guile sonic boom, etc)
P1_combo_move_step[34] = -1 -- Charge right 2 seconds, left + fierce punch (guile sonic boom, etc)

P1_combo_move_step[35] = -1 -- Charge left+down 2 seconds, right + jab punch (stay planted, also longer charge for e honda, blanka)
P1_combo_move_step[36] = -1 -- Charge left+down 2 seconds, right + strong punch (stay planted, also longer charge for e honda, blanka)
P1_combo_move_step[37] = -1 -- Charge left+down 2 seconds, right + fierce punch (stay planted, also longer charge for e honda, blanka)

P1_combo_move_step[38] = -1 -- Charge right+down 2 seconds, left + jab punch (stay planted, also longer charge for e honda, blanka)
P1_combo_move_step[39] = -1 -- Charge right+down 2 seconds, left + strong punch (stay planted, also longer charge for e honda, blanka)
P1_combo_move_step[40] = -1 -- Charge right+down 2 seconds, left + fierce punch (stay planted, also longer charge for e honda, blanka)

P1_combo_move_step[41] = -1 -- Rapidly press punch
P1_combo_move_step[42] = -1 -- Rapidly press kick

P1_combo_move_step[43] = -1 -- All 3 punches
P1_combo_move_step[44] = -1 -- All 3 kicks

P1_combo_move_step[45] = -1 -- Chrage all punches 1 second (balrog turn around punch)

P1_combo_move_step[46] = -1 -- 360 clockwise, fierce punch (zangief piledriver)
P1_combo_move_step[47] = -1 -- 360 counter-clockwise, fierce punch (zangief piledriver)

P1_combo_move_step[48] = -1 -- Left, Down, Left-Down + All Punches (dhalsim)
P1_combo_move_step[49] = -1 -- Right, Down, Right-Down + All Punches (dhalsim)

P1_combo_move_step[50] = -1 -- Left, Down-Left, Down, Down-Right, Right + Jab Punch (half circle fireball motion)
P1_combo_move_step[51] = -1 -- Left, Down-Left, Down, Down-Right, Right + Strong Punch (half circle fireball motion)
P1_combo_move_step[52] = -1 -- Left, Down-Left, Down, Down-Right, Right + Fierce Punch (half circle fireball motion)

P1_combo_move_step[53] = -1 -- Right, Down-Right, Down, Down-Left, Left + Jab Punch (half circle fireball motion)
P1_combo_move_step[54] = -1 -- Right, Down-Right, Down, Down-Left, Left + Strong Punch (half circle fireball motion)
P1_combo_move_step[55] = -1 -- Right, Down-Right, Down, Down-Left, Left + Fierce Punch (half circle fireball motion)

P1_combo_move_step[56] = -1 -- Down, Up-Right + Short Kick 
P1_combo_move_step[57] = -1 -- Down, Up-Right + Forward Kick
P1_combo_move_step[58] = -1 -- Down, Up-Right + Roundhouse Kick

P1_combo_move_step[59] = -1 -- Down, Up-Left + Short Kick
P1_combo_move_step[60] = -1 -- Down, Up-Left + Forward Kick
P1_combo_move_step[61] = -1 -- Down, Up-Left + Roundhouse Kick

P1_combo_move_step[62] = -1 -- Down, down-right, right, short kick (fireball motion)
P1_combo_move_step[63] = -1 -- Down, down-right, right, forward kick (fireball motion)
P1_combo_move_step[64] = -1 -- Down, down-right, right, roundhouse kick (fireball motion)

P1_combo_move_step[65] = -1 -- Down, down-left, left, short kick (fireball motion)
P1_combo_move_step[66] = -1 -- Down, down-left, left, forward kick (fireball motion)
P1_combo_move_step[67] = -1 -- Down, down-left, left, roundhouse kick (fireball motion)

P1_combo_move_step[68] = -1 -- Charge left 2 seconds, right + short kick (balrog uppercut, bison scissor kick)
P1_combo_move_step[69] = -1 -- Charge left 2 seconds, right + forward kick (balrog uppercut, bison scissor kick)
P1_combo_move_step[70] = -1 -- Charge left 2 seconds, right + roundhouse kick (balrog uppercut, bison scissor kick)

P1_combo_move_step[71] = -1 -- Charge right 2 seconds, left + short kick (balrog uppercut, bison scissor kick)
P1_combo_move_step[72] = -1 -- Charge right 2 seconds, left + forward kick (balrog uppercut, bison scissor kick)
P1_combo_move_step[73] = -1 -- Charge right 2 seconds, left + roundhouse kick (balrog uppercut, bison scissor kick)

P1_combo_move_step[74] = -1 -- Charge down 2 seconds, up + short kick (guile, bison, chun li, etc)
P1_combo_move_step[75] = -1 -- Charge down 2 seconds, up + forward kick (guile, bison, chun li, etc)
P1_combo_move_step[76] = -1 -- Charge down 2 seconds, up + roundhouse kick (guile, bison, chun li, etc)

P1_combo_move_step[77] = -1 -- Charge down 3 seconds, up + short kick (e honda)
P1_combo_move_step[78] = -1 -- Charge down 3 seconds, up + forward kick (e honda)
P1_combo_move_step[79] = -1 -- Charge down 3 seconds, up + roundhouse kick (e honda)




P2_combo_move_step[11] = -1 -- Down, down-right, right, jab punch (fireball motion)
P2_combo_move_step[12] = -1 -- Right, down, down-right, jab punch (dragon punch motion)	
P2_combo_move_step[13] = -1 -- Down, down-left, left, short kick (hurricane kick motion)

P2_combo_move_step[14] = -1 -- Down, down-left, left, jab punch (fireball motion)
P2_combo_move_step[15] = -1 -- left, down, down-left, jab punch (dragon punch motion)	
P2_combo_move_step[16] = -1 -- Down, down-right, right, short kick (hurricane kick motion)

P2_combo_move_step[17] = -1 -- Down, down-right, right, strong punch (fireball motion)
P2_combo_move_step[18] = -1 -- Right, down, down-right, strong punch (dragon punch motion)	
P2_combo_move_step[19] = -1 -- Down, down-left, left, forward kick (hurricane kick motion)

P2_combo_move_step[20] = -1 -- Down, down-left, left, strong punch (fireball motion)
P2_combo_move_step[21] = -1 -- left, down, down-left, strong punch (dragon punch motion)	
P2_combo_move_step[22] = -1 -- Down, down-right, right, forward kick (hurricane kick motion)

P2_combo_move_step[23] = -1 -- Down, down-right, right, fierce punch (fireball motion)
P2_combo_move_step[24] = -1 -- Right, down, down-right, fierce punch (dragon punch motion)	
P2_combo_move_step[25] = -1 -- Down, down-left, left, roundhouse kick (hurricane kick motion)

P2_combo_move_step[26] = -1 -- Down, down-left, left, fierce punch (fireball motion)
P2_combo_move_step[27] = -1 -- left, down, down-left, fierce punch (dragon punch motion)	
P2_combo_move_step[28] = -1 -- Down, down-right, right, roundhouse kick (hurricane kick motion)

P2_combo_move_step[29] = -1 -- Charge left 2 seconds, right + jab punch (guile sonic boom, etc)
P2_combo_move_step[30] = -1 -- Charge left 2 seconds, right + strong punch (guile sonic boom, etc)
P2_combo_move_step[31] = -1 -- Charge left 2 seconds, right + fierce punch (guile sonic boom, etc)

P2_combo_move_step[32] = -1 -- Charge right 2 seconds, left + jab punch (guile sonic boom, etc)
P2_combo_move_step[33] = -1 -- Charge right 2 seconds, left + strong punch (guile sonic boom, etc)
P2_combo_move_step[34] = -1 -- Charge right 2 seconds, left + fierce punch (guile sonic boom, etc)

P2_combo_move_step[35] = -1 -- Charge left+down 2 seconds, right + jab punch (guile sonic boom, etc, stay crouched)
P2_combo_move_step[36] = -1 -- Charge left+down 2 seconds, right + strong punch (guile sonic boom, etc, stay crouched)
P2_combo_move_step[37] = -1 -- Charge left+down 2 seconds, right + fierce punch (guile sonic boom, etc, stay crouched)

P2_combo_move_step[38] = -1 -- Charge right+down 2 seconds, left + jab punch (guile sonic boom, etc, stay crouched)
P2_combo_move_step[39] = -1 -- Charge right+down 2 seconds, left + strong punch (guile sonic boom, etc, stay crouched)
P2_combo_move_step[40] = -1 -- Charge right+down 2 seconds, left + fierce punch (guile sonic boom, etc, stay crouched)

P2_combo_move_step[41] = -1 -- Rapidly press punch
P2_combo_move_step[42] = -1 -- Rapidly press kick

P2_combo_move_step[43] = -1 -- All 3 punches
P2_combo_move_step[44] = -1 -- All 3 kicks

P2_combo_move_step[45] = -1 -- Chrage all punches 1 second (balrog turn around punch)

P2_combo_move_step[46] = -1 -- 360 clockwise, fierce punch (zangief piledriver)
P2_combo_move_step[47] = -1 -- 360 counter-clockwise, fierce punch (zangief piledriver)

P2_combo_move_step[48] = -1 -- Left, Down, Down-Left + All Punches (dhalsim)
P2_combo_move_step[49] = -1 -- Right, Down, Down-Right + All Punches (dhalsim)

P2_combo_move_step[50] = -1 -- Left, Down-Left, Down, Down-Right, Right + Jab Punch (half circle fireball motion)
P2_combo_move_step[51] = -1 -- Left, Down-Left, Down, Down-Right, Right + Strong Punch (half circle fireball motion)
P2_combo_move_step[52] = -1 -- Left, Down-Left, Down, Down-Right, Right + Fierce Punch (half circle fireball motion)

P2_combo_move_step[53] = -1 -- Right, Down-Right, Down, Down-Left, Left + Jab Punch (half circle fireball motion)
P2_combo_move_step[54] = -1 -- Right, Down-Right, Down, Down-Left, Left + Strong Punch (half circle fireball motion)
P2_combo_move_step[55] = -1 -- Right, Down-Right, Down, Down-Left, Left + Fierce Punch (half circle fireball motion)

P2_combo_move_step[56] = -1 -- Down, Up-Right + Short Kick 
P2_combo_move_step[57] = -1 -- Down, Up-Right + Forward Kick
P2_combo_move_step[58] = -1 -- Down, Up-Right + Roundhouse Kick

P2_combo_move_step[59] = -1 -- Down, Up-Left + Short Kick
P2_combo_move_step[60] = -1 -- Down, Up-Left + Forward Kick
P2_combo_move_step[61] = -1 -- Down, Up-Left + Roundhouse Kick

P2_combo_move_step[62] = -1 -- Down, down-right, right, short kick (fireball motion)
P2_combo_move_step[63] = -1 -- Down, down-right, right, forward kick (fireball motion)
P2_combo_move_step[64] = -1 -- Down, down-right, right, roundhouse kick (fireball motion)

P2_combo_move_step[65] = -1 -- Down, down-left, left, short kick (fireball motion)
P2_combo_move_step[66] = -1 -- Down, down-left, left, forward kick (fireball motion)
P2_combo_move_step[67] = -1 -- Down, down-left, left, roundhouse kick (fireball motion)

P2_combo_move_step[68] = -1 -- Charge left 2 seconds, right + short kick (balrog uppercut, bison scissor kick)
P2_combo_move_step[69] = -1 -- Charge left 2 seconds, right + forward kick (balrog uppercut, bison scissor kick)
P2_combo_move_step[70] = -1 -- Charge left 2 seconds, right + roundhouse kick (balrog uppercut, bison scissor kick)

P2_combo_move_step[71] = -1 -- Charge right 2 seconds, left + short kick (balrog uppercut, bison scissor kick)
P2_combo_move_step[72] = -1 -- Charge right 2 seconds, left + forward kick (balrog uppercut, bison scissor kick)
P2_combo_move_step[73] = -1 -- Charge right 2 seconds, left + roundhouse kick (balrog uppercut, bison scissor kick)

P2_combo_move_step[74] = -1 -- Charge down 2 seconds, up + short kick (guile, bison, chun li, etc)
P2_combo_move_step[75] = -1 -- Charge down 2 seconds, up + forward kick (guile, bison, chun li, etc)
P2_combo_move_step[76] = -1 -- Charge down 2 seconds, up + roundhouse kick (guile, bison, chun li, etc)

P2_combo_move_step[77] = -1 -- Charge down 3 seconds, up + short kick (e honda)
P2_combo_move_step[78] = -1 -- Charge down 3 seconds, up + forward kick (e honda)
P2_combo_move_step[79] = -1 -- Charge down 3 seconds, up + roundhouse kick (e honda)







--
-- ComboMoves:CheckComboMoves() - Checks to see if any combo moves are in Progress
--  Returns -1 if no combo moves are in progress, otherwise returns the P1_combo_move_step index of the active move
--
function ComboMoves:CheckComboMoves(playernum)
	retval = -1

	if playernum == 1 then
		for i = combo_index_start, combo_index_end do
			if P1_combo_move_step[i] > 0 then
				retval = i
				break
			end
		end
	else -- player 2
		for i = combo_index_start, combo_index_end do
			if P2_combo_move_step[i] > 0 then
				retval = i
				break
			end
		end		
	end

	return retval
end






--
-- ComboMoves:ProcessComboMoves() - Processes the combo move in progress, injecting the appropriate actions
--
function ComboMoves:ProcessComboMoves(index, playernum)
	alldone = false

	if playernum == 1 then
		alldone = false
		if index == 11 then -- fireball right
			if P1_combo_move_step[11] == 1 then -- Starting a new combo move
				P1_combo_move_step[11] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[11] == 2 then -- Continuing this combo move
				P1_combo_move_step[11] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[11] == 3 then -- Continuing this combo move
				P1_combo_move_step[11] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[11] == 4 then -- Continuing this combo move
				P1_combo_move_step[11] = 5
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[11] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 12 then -- dragon punch right
			if P1_combo_move_step[12] == 1 then -- Starting a new combo move
				P1_combo_move_step[12] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[12] == 2 then -- Continuing this combo move
				P1_combo_move_step[12] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[12] == 3 then -- Continuing this combo move
				P1_combo_move_step[12] = 4
				AIButtons["P1 Down"].pressed = true				
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[12] == 4 then -- Continuing this combo move
				P1_combo_move_step[12] = 5
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[12] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 13 then -- hurricane kick right
			if P1_combo_move_step[13] == 1 then -- Starting a new combo move
				P1_combo_move_step[13] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[13] == 2 then -- Continuing this combo move
				P1_combo_move_step[13] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true				
			elseif P1_combo_move_step[13] == 3 then -- Continuing this combo move
				P1_combo_move_step[13] = 4
				AIButtons["P1 Left"].pressed = true				
			elseif P1_combo_move_step[13] == 4 then -- Continuing this combo move
				P1_combo_move_step[13] = 5
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[13] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 14 then -- fireball left
			if P1_combo_move_step[14] == 1 then -- Starting a new combo move
				P1_combo_move_step[14] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[14] == 2 then -- Continuing this combo move
				P1_combo_move_step[14] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[14] == 3 then -- Continuing this combo move
				P1_combo_move_step[14] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[14] == 4 then -- Continuing this combo move
				P1_combo_move_step[14] = 5
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[14] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 15 then -- dragon punch left
			if P1_combo_move_step[15] == 1 then -- Starting a new combo move
				P1_combo_move_step[15] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[15] == 2 then -- Continuing this combo move
				P1_combo_move_step[15] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[15] == 3 then -- Continuing this combo move
				P1_combo_move_step[15] = 4
				AIButtons["P1 Down"].pressed = true				
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[15] == 4 then -- Continuing this combo move
				P1_combo_move_step[15] = 5
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[15] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 16 then -- hurricane kick left
			if P1_combo_move_step[16] == 1 then -- Starting a new combo move
				P1_combo_move_step[16] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[16] == 2 then -- Continuing this combo move
				P1_combo_move_step[16] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true				
			elseif P1_combo_move_step[16] == 3 then -- Continuing this combo move
				P1_combo_move_step[16] = 4
				AIButtons["P1 Right"].pressed = true				
			elseif P1_combo_move_step[16] == 4 then -- Continuing this combo move
				P1_combo_move_step[16] = 5
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[16] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 17 then -- fireball right
			if P1_combo_move_step[17] == 1 then -- Starting a new combo move
				P1_combo_move_step[17] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[17] == 2 then -- Continuing this combo move
				P1_combo_move_step[17] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[17] == 3 then -- Continuing this combo move
				P1_combo_move_step[17] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[17] == 4 then -- Continuing this combo move
				P1_combo_move_step[17] = 5
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[17] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 18 then -- dragon punch right
			if P1_combo_move_step[18] == 1 then -- Starting a new combo move
				P1_combo_move_step[18] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[18] == 2 then -- Continuing this combo move
				P1_combo_move_step[18] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[18] == 3 then -- Continuing this combo move
				P1_combo_move_step[18] = 4
				AIButtons["P1 Down"].pressed = true				
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[18] == 4 then -- Continuing this combo move
				P1_combo_move_step[18] = 5
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[18] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 19 then -- hurricane kick right
			if P1_combo_move_step[19] == 1 then -- Starting a new combo move
				P1_combo_move_step[19] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[19] == 2 then -- Continuing this combo move
				P1_combo_move_step[19] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true				
			elseif P1_combo_move_step[19] == 3 then -- Continuing this combo move
				P1_combo_move_step[19] = 4
				AIButtons["P1 Left"].pressed = true				
			elseif P1_combo_move_step[19] == 4 then -- Continuing this combo move
				P1_combo_move_step[19] = 5
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[19] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 20 then -- fireball left
			if P1_combo_move_step[20] == 1 then -- Starting a new combo move
				P1_combo_move_step[20] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[20] == 2 then -- Continuing this combo move
				P1_combo_move_step[20] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[20] == 3 then -- Continuing this combo move
				P1_combo_move_step[20] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[20] == 4 then -- Continuing this combo move
				P1_combo_move_step[20] = 5
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[20] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 21 then -- dragon punch left
			if P1_combo_move_step[21] == 1 then -- Starting a new combo move
				P1_combo_move_step[21] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[21] == 2 then -- Continuing this combo move
				P1_combo_move_step[21] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[21] == 3 then -- Continuing this combo move
				P1_combo_move_step[21] = 4
				AIButtons["P1 Down"].pressed = true				
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[21] == 4 then -- Continuing this combo move
				P1_combo_move_step[21] = 5
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[21] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 22 then -- hurricane kick left
			if P1_combo_move_step[22] == 1 then -- Starting a new combo move
				P1_combo_move_step[22] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[22] == 2 then -- Continuing this combo move
				P1_combo_move_step[22] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true				
			elseif P1_combo_move_step[22] == 3 then -- Continuing this combo move
				P1_combo_move_step[22] = 4
				AIButtons["P1 Right"].pressed = true				
			elseif P1_combo_move_step[22] == 4 then -- Continuing this combo move
				P1_combo_move_step[22] = 5
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[22] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 23 then -- fireball right
			if P1_combo_move_step[23] == 1 then -- Starting a new combo move
				P1_combo_move_step[23] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[23] == 2 then -- Continuing this combo move
				P1_combo_move_step[23] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[23] == 3 then -- Continuing this combo move
				P1_combo_move_step[23] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[23] == 4 then -- Continuing this combo move
				P1_combo_move_step[23] = 5
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[23] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 24 then -- dragon punch right
			if P1_combo_move_step[24] == 1 then -- Starting a new combo move
				P1_combo_move_step[24] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[24] == 2 then -- Continuing this combo move
				P1_combo_move_step[24] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[24] == 3 then -- Continuing this combo move
				P1_combo_move_step[24] = 4
				AIButtons["P1 Down"].pressed = true				
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[24] == 4 then -- Continuing this combo move
				P1_combo_move_step[24] = 5
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[24] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 25 then -- hurricane kick right
			if P1_combo_move_step[25] == 1 then -- Starting a new combo move
				P1_combo_move_step[25] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[25] == 2 then -- Continuing this combo move
				P1_combo_move_step[25] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true				
			elseif P1_combo_move_step[25] == 3 then -- Continuing this combo move
				P1_combo_move_step[25] = 4
				AIButtons["P1 Left"].pressed = true				
			elseif P1_combo_move_step[25] == 4 then -- Continuing this combo move
				P1_combo_move_step[25] = 5
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[25] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 26 then -- fireball left
			if P1_combo_move_step[26] == 1 then -- Starting a new combo move
				P1_combo_move_step[26] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[26] == 2 then -- Continuing this combo move
				P1_combo_move_step[26] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[26] == 3 then -- Continuing this combo move
				P1_combo_move_step[26] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[26] == 4 then -- Continuing this combo move
				P1_combo_move_step[26] = 5
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[26] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 27 then -- dragon punch left
			if P1_combo_move_step[27] == 1 then -- Starting a new combo move
				P1_combo_move_step[27] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[27] == 2 then -- Continuing this combo move
				P1_combo_move_step[27] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[27] == 3 then -- Continuing this combo move
				P1_combo_move_step[27] = 4
				AIButtons["P1 Down"].pressed = true				
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[27] == 4 then -- Continuing this combo move
				P1_combo_move_step[27] = 5
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[27] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 28 then -- hurricane kick left
			if P1_combo_move_step[28] == 1 then -- Starting a new combo move
				P1_combo_move_step[28] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[28] == 2 then -- Continuing this combo move
				P1_combo_move_step[28] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true				
			elseif P1_combo_move_step[28] == 3 then -- Continuing this combo move
				P1_combo_move_step[28] = 4
				AIButtons["P1 Right"].pressed = true				
			elseif P1_combo_move_step[28] == 4 then -- Continuing this combo move
				P1_combo_move_step[28] = 5
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[28] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 29 then -- Charge left 2 seconds, right + jab punch (guile sonic boom, etc)
			if P1_combo_move_step[29] == 1 then -- Starting a new combo move
				P1_combo_move_step[29] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[29] >= 2 and P1_combo_move_step[29] <= 35 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				P1_combo_move_step[29] = P1_combo_move_step[29] + 1
			elseif P1_combo_move_step[29] == 36 then -- Continuing this combo move	
				P1_combo_move_step[29] = 37		
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[29] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P1_combo_move_step[29] = 38		
				AIButtons["P1 Right"].pressed = true	
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[29] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 30 then -- Charge left 2 seconds, right + strong punch (guile sonic boom, etc)
			if P1_combo_move_step[30] == 1 then -- Starting a new combo move
				P1_combo_move_step[30] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[30] >= 2 and P1_combo_move_step[30] <= 35 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				P1_combo_move_step[30] = P1_combo_move_step[30] + 1
			elseif P1_combo_move_step[30] == 36 then -- Continuing this combo move
				P1_combo_move_step[30] = 37
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[30] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P1_combo_move_step[30] = 38
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[30] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 31 then -- Charge left 2 seconds, right + fierce punch (guile sonic boom, etc)
			if P1_combo_move_step[31] == 1 then -- Starting a new combo move
				P1_combo_move_step[31] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[31] >= 2 and P1_combo_move_step[31] <= 35 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				P1_combo_move_step[31] = P1_combo_move_step[31] + 1
			elseif P1_combo_move_step[31] == 36 then -- Continuing this combo move
				P1_combo_move_step[31] = 37
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[31] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P1_combo_move_step[31] = 38								
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[31] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 32 then -- Charge right 2 seconds, left + jab punch (guile sonic boom, etc)
			if P1_combo_move_step[32] == 1 then -- Starting a new combo move
				P1_combo_move_step[32] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[32] >= 2 and P1_combo_move_step[32] <= 35 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				P1_combo_move_step[32] = P1_combo_move_step[32] + 1
			elseif P1_combo_move_step[32] == 36 then -- Continuing this combo move
				P1_combo_move_step[32] = 37
				AIButtons["P1 Left"].pressed = true		
			elseif P1_combo_move_step[32] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P1_combo_move_step[32] = 38						
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[32] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 33 then -- Charge right 2 seconds, left + strong punch (guile sonic boom, etc)
			if P1_combo_move_step[33] == 1 then -- Starting a new combo move
				P1_combo_move_step[33] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[33] >= 2 and P1_combo_move_step[33] <= 35 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				P1_combo_move_step[33] = P1_combo_move_step[33] + 1
			elseif P1_combo_move_step[33] == 36 then -- Continuing this combo move
				P1_combo_move_step[33] = 37
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[33] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P1_combo_move_step[33] = 38							
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[33] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 34 then -- Charge right 2 seconds, left + fierce punch (guile sonic boom, etc)
			if P1_combo_move_step[34] == 1 then -- Starting a new combo move
				P1_combo_move_step[34] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[34] >= 2 and P1_combo_move_step[34] <= 35 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				P1_combo_move_step[34] = P1_combo_move_step[34] + 1
			elseif P1_combo_move_step[34] == 36 then -- Continuing this combo move
				P1_combo_move_step[34] = 37
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[34] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P1_combo_move_step[34] = 38							
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[34] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 35 then -- Charge left+down 2 seconds, right + jab punch (guile sonic boom, etc)
			if P1_combo_move_step[35] == 1 then -- Starting a new combo move
				P1_combo_move_step[35] = 2
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[35] >= 2 and P1_combo_move_step[35] <= 31 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Down"].pressed = true				
				P1_combo_move_step[35] = P1_combo_move_step[35] + 1
			elseif P1_combo_move_step[35] == 32 then -- Continuing this combo move
				P1_combo_move_step[35] = 33
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[35] == 33 then -- Continuing this combo move	
				P1_combo_move_step[35] = 34		
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[35] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P1_combo_move_step[35] = 35		
				AIButtons["P1 Right"].pressed = true	
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[35] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 36 then -- Charge left+down 2 seconds, right + strong punch (guile sonic boom, etc)
			if P1_combo_move_step[36] == 1 then -- Starting a new combo move
				P1_combo_move_step[36] = 2
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[36] >= 2 and P1_combo_move_step[36] <= 31 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Down"].pressed = true				
				P1_combo_move_step[36] = P1_combo_move_step[36] + 1
			elseif P1_combo_move_step[36] == 32 then -- Continuing this combo move
				P1_combo_move_step[36] = 33
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[36] == 33 then -- Continuing this combo move	
				P1_combo_move_step[36] = 34		
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[36] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P1_combo_move_step[36] = 35		
				AIButtons["P1 Right"].pressed = true	
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[36] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 37 then -- Charge left+down 2 seconds, right + fierce punch (guile sonic boom, etc)
			if P1_combo_move_step[37] == 1 then -- Starting a new combo move
				P1_combo_move_step[37] = 2
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[37] >= 2 and P1_combo_move_step[37] <= 31 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Down"].pressed = true				
				P1_combo_move_step[37] = P1_combo_move_step[37] + 1
			elseif P1_combo_move_step[37] == 32 then -- Continuing this combo move
				P1_combo_move_step[37] = 33
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[37] == 33 then -- Continuing this combo move	
				P1_combo_move_step[37] = 34		
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[37] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P1_combo_move_step[37] = 35		
				AIButtons["P1 Right"].pressed = true	
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[37] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 38 then -- Charge right+down 2 seconds, left + jab punch (guile sonic boom, etc)
			if P1_combo_move_step[38] == 1 then -- Starting a new combo move
				P1_combo_move_step[38] = 2
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[38] >= 2 and P1_combo_move_step[38] <= 31 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Down"].pressed = true				
				P1_combo_move_step[38] = P1_combo_move_step[38] + 1
			elseif P1_combo_move_step[38] == 32 then -- Continuing this combo move
				P1_combo_move_step[38] = 33
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[38] == 33 then -- Continuing this combo move	
				P1_combo_move_step[38] = 34		
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[38] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P1_combo_move_step[38] = 35		
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[38] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 39 then -- Charge right+down 2 seconds, left + strong punch (guile sonic boom, etc)
			if P1_combo_move_step[39] == 1 then -- Starting a new combo move
				P1_combo_move_step[39] = 2
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[39] >= 2 and P1_combo_move_step[39] <= 31 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Down"].pressed = true				
				P1_combo_move_step[39] = P1_combo_move_step[39] + 1
			elseif P1_combo_move_step[39] == 32 then -- Continuing this combo move
				P1_combo_move_step[39] = 33
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[39] == 33 then -- Continuing this combo move	
				P1_combo_move_step[39] = 34		
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[39] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P1_combo_move_step[39] = 35		
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[39] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 40 then -- Charge right+down 2 seconds, left + fierce punch (guile sonic boom, etc)
			if P1_combo_move_step[40] == 1 then -- Starting a new combo move
				P1_combo_move_step[40] = 2
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[40] >= 2 and P1_combo_move_step[40] <= 31 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Down"].pressed = true				
				P1_combo_move_step[40] = P1_combo_move_step[40] + 1
			elseif P1_combo_move_step[40] == 32 then -- Continuing this combo move
				P1_combo_move_step[40] = 33
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[40] == 33 then -- Continuing this combo move	
				P1_combo_move_step[40] = 34		
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[40] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P1_combo_move_step[40] = 35		
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[40] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 41 then -- Rapidly press punch
			if P1_combo_move_step[41] == 1 then -- Starting a new combo move
				P1_combo_move_step[41] = 2
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[41] >= 2 and P1_combo_move_step[41] <= 59 then -- Continuing this combo move
				if P1_combo_move_step[41] % 2 == 0 then -- Only press every other frame
					AIButtons["P1 Jab Punch"].pressed = true
				end
				P1_combo_move_step[41] = P1_combo_move_step[41] + 1
			elseif P1_combo_move_step[41] == 60 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 42 then -- Rapidly press kick
			if P1_combo_move_step[42] == 1 then -- Starting a new combo move
				P1_combo_move_step[42] = 2
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[42] >= 2 and P1_combo_move_step[42] <= 59 then -- Continuing this combo move
				if P1_combo_move_step[42] % 2 == 0 then -- Only press every other frame
					AIButtons["P1 Short Kick"].pressed = true
				end
				P1_combo_move_step[42] = P1_combo_move_step[42] + 1
			elseif P1_combo_move_step[42] == 60 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 43 then -- All 3 punches
			if P1_combo_move_step[43] >= 1 and P1_combo_move_step[43] <= 4 then -- 4 frames are required to register
				AIButtons["P1 Jab Punch"].pressed = true
				AIButtons["P1 Strong Punch"].pressed = true
				AIButtons["P1 Fierce Punch"].pressed = true
				P1_combo_move_step[43] = P1_combo_move_step[43] + 1
			elseif P1_combo_move_step[43] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 44 then -- All 3 kicks
			if P1_combo_move_step[44] >= 1 and P1_combo_move_step[44] <= 4 then -- 4 frames are required to register
				AIButtons["P1 Short Kick"].pressed = true
				AIButtons["P1 Forward Kick"].pressed = true
				AIButtons["P1 Roundhouse Kick"].pressed = true
				P1_combo_move_step[44] = P1_combo_move_step[44] + 1
			elseif P1_combo_move_step[44] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 45 then -- All 3 punches
			if P1_combo_move_step[45] >= 1 and P1_combo_move_step[45] <= 30 then -- 1 second charge
				AIButtons["P1 Jab Punch"].pressed = true
				AIButtons["P1 Strong Punch"].pressed = true
				AIButtons["P1 Fierce Punch"].pressed = true
				P1_combo_move_step[45] = P1_combo_move_step[45] + 1
			elseif P1_combo_move_step[45] == 31 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 46 then -- 360 clockwise (forward, down, back, up + punch)
			if P1_combo_move_step[46] == 1 then -- Starting a new combo move
				P1_combo_move_step[46] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[46] == 2 then -- Continuing this combo move
				P1_combo_move_step[46] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[46] == 3 then -- Continuing this combo move
				P1_combo_move_step[46] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[46] == 4 then -- Continuing this combo move
				P1_combo_move_step[46] = 5
				AIButtons["P1 Up"].pressed = true
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[46] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 47 then -- 360 counter-clockwise (forward, down, back, up + punch)
			if P1_combo_move_step[47] == 1 then -- Starting a new combo move
				P1_combo_move_step[47] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[47] == 2 then -- Continuing this combo move
				P1_combo_move_step[47] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[47] == 3 then -- Continuing this combo move
				P1_combo_move_step[47] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[47] == 4 then -- Continuing this combo move
				P1_combo_move_step[47] = 5
				AIButtons["P1 Up"].pressed = true
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[47] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 48 then -- dragon punch left + all 3 punches
			if P1_combo_move_step[48] == 1 then -- Starting a new combo move
				P1_combo_move_step[48] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[48] == 2 then -- Continuing this combo move
				P1_combo_move_step[48] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[48] == 3 then -- Continuing this combo move
				P1_combo_move_step[48] = 4
				AIButtons["P1 Down"].pressed = true				
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[48] == 4 then -- Continuing this combo move
				P1_combo_move_step[48] = 5
				AIButtons["P1 Jab Punch"].pressed = true
				AIButtons["P1 Strong Punch"].pressed = true
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[48] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 49 then -- dragon punch right + all 3 punches
			if P1_combo_move_step[49] == 1 then -- Starting a new combo move
				P1_combo_move_step[49] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[49] == 2 then -- Continuing this combo move
				P1_combo_move_step[49] = 3
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[49] == 3 then -- Continuing this combo move
				P1_combo_move_step[49] = 4
				AIButtons["P1 Down"].pressed = true				
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[49] == 4 then -- Continuing this combo move
				P1_combo_move_step[49] = 5
				AIButtons["P1 Jab Punch"].pressed = true
				AIButtons["P1 Strong Punch"].pressed = true
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[49] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 50 then -- half circle fireball right
			if P1_combo_move_step[50] == 1 then -- Starting a new combo move
				P1_combo_move_step[50] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[50] == 2 then -- Continuing this combo move
				P1_combo_move_step[50] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[50] == 3 then -- Continuing this combo move
				P1_combo_move_step[50] = 4
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[50] == 4 then -- Continuing this combo move
				P1_combo_move_step[50] = 5
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[50] == 5 then -- Continuing this combo move
				P1_combo_move_step[50] = 6
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[50] == 6 then -- Continuing this combo move
				P1_combo_move_step[50] = 7
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[50] == 7 then -- Cooldown frame
				alldone = true
			end


		elseif index == 51 then -- half circle fireball right
			if P1_combo_move_step[51] == 1 then -- Starting a new combo move
				P1_combo_move_step[51] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[51] == 2 then -- Continuing this combo move
				P1_combo_move_step[51] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[51] == 3 then -- Continuing this combo move
				P1_combo_move_step[51] = 4
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[51] == 4 then -- Continuing this combo move
				P1_combo_move_step[51] = 5
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[51] == 5 then -- Continuing this combo move
				P1_combo_move_step[51] = 6
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[51] == 6 then -- Continuing this combo move
				P1_combo_move_step[51] = 7
				AIButtons["P1 Strong Punch"].pressed = true
			elseif P1_combo_move_step[51] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 52 then -- half circle fireball right
			if P1_combo_move_step[52] == 1 then -- Starting a new combo move
				P1_combo_move_step[52] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[52] == 2 then -- Continuing this combo move
				P1_combo_move_step[52] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[52] == 3 then -- Continuing this combo move
				P1_combo_move_step[52] = 4
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[52] == 4 then -- Continuing this combo move
				P1_combo_move_step[52] = 5
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[52] == 5 then -- Continuing this combo move
				P1_combo_move_step[52] = 6
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[52] == 6 then -- Continuing this combo move
				P1_combo_move_step[52] = 7
				AIButtons["P1 Fierce Punch"].pressed = true
			elseif P1_combo_move_step[52] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 53 then -- half circle fireball left
			if P1_combo_move_step[53] == 1 then -- Starting a new combo move
				P1_combo_move_step[53] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[53] == 2 then -- Continuing this combo move
				P1_combo_move_step[53] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[53] == 3 then -- Continuing this combo move
				P1_combo_move_step[53] = 4
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[53] == 4 then -- Continuing this combo move
				P1_combo_move_step[53] = 5
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[53] == 5 then -- Continuing this combo move
				P1_combo_move_step[53] = 6
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[53] == 6 then -- Continuing this combo move
				P1_combo_move_step[53] = 7
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[53] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 54 then -- half circle fireball left
			if P1_combo_move_step[54] == 1 then -- Starting a new combo move
				P1_combo_move_step[54] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[54] == 2 then -- Continuing this combo move
				P1_combo_move_step[54] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[54] == 3 then -- Continuing this combo move
				P1_combo_move_step[54] = 4
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[54] == 4 then -- Continuing this combo move
				P1_combo_move_step[54] = 5
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[54] == 5 then -- Continuing this combo move
				P1_combo_move_step[54] = 6
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[54] == 6 then -- Continuing this combo move
				P1_combo_move_step[54] = 7
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[54] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 55 then -- half circle fireball left
			if P1_combo_move_step[55] == 1 then -- Starting a new combo move
				P1_combo_move_step[55] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[55] == 2 then -- Continuing this combo move
				P1_combo_move_step[55] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[55] == 3 then -- Continuing this combo move
				P1_combo_move_step[55] = 4
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[55] == 4 then -- Continuing this combo move
				P1_combo_move_step[55] = 5
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[55] == 5 then -- Continuing this combo move
				P1_combo_move_step[55] = 6
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[55] == 6 then -- Continuing this combo move
				P1_combo_move_step[55] = 7
				AIButtons["P1 Jab Punch"].pressed = true
			elseif P1_combo_move_step[55] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 56 then -- down, down-right, right, up-right + kick
			if P1_combo_move_step[56] == 1 then -- Starting a new combo move
				P1_combo_move_step[56] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[56] == 2 then -- Continuing this combo move
				P1_combo_move_step[56] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[56] == 3 then -- Continuing this combo move
				P1_combo_move_step[56] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[56] == 4 then -- Continuing this combo move
				P1_combo_move_step[56] = 5
				AIButtons["P1 Up"].pressed = true				
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[56] == 5 then -- Continuing this combo move
				P1_combo_move_step[56] = 6
			elseif P1_combo_move_step[56] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 57 then -- down, down-right, right, up-right + kick
			if P1_combo_move_step[57] == 1 then -- Starting a new combo move
				P1_combo_move_step[57] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[57] == 2 then -- Continuing this combo move
				P1_combo_move_step[57] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[57] == 3 then -- Continuing this combo move
				P1_combo_move_step[57] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[57] == 4 then -- Continuing this combo move
				P1_combo_move_step[57] = 5
				AIButtons["P1 Up"].pressed = true				
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[57] == 5 then -- Continuing this combo move
				P1_combo_move_step[57] = 6
			elseif P1_combo_move_step[57] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 58 then -- down, down-right, right, up-right + kick
			if P1_combo_move_step[58] == 1 then -- Starting a new combo move
				P1_combo_move_step[58] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[58] == 2 then -- Continuing this combo move
				P1_combo_move_step[58] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[58] == 3 then -- Continuing this combo move
				P1_combo_move_step[58] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[58] == 4 then -- Continuing this combo move
				P1_combo_move_step[58] = 5
				AIButtons["P1 Up"].pressed = true				
				AIButtons["P1 Right"].pressed = true
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[58] == 5 then -- Continuing this combo move
				P1_combo_move_step[58] = 6
			elseif P1_combo_move_step[58] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 59 then -- down, down-left, left, up-left + kick
			if P1_combo_move_step[59] == 1 then -- Starting a new combo move
				P1_combo_move_step[59] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[59] == 2 then -- Continuing this combo move
				P1_combo_move_step[59] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[59] == 3 then -- Continuing this combo move
				P1_combo_move_step[59] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[59] == 4 then -- Continuing this combo move
				P1_combo_move_step[59] = 5
				AIButtons["P1 Up"].pressed = true				
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[59] == 5 then -- Continuing this combo move
				P1_combo_move_step[59] = 6
			elseif P1_combo_move_step[59] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 60 then -- down, down-left, left, up-left + kick
			if P1_combo_move_step[60] == 1 then -- Starting a new combo move
				P1_combo_move_step[60] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[60] == 2 then -- Continuing this combo move
				P1_combo_move_step[60] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[60] == 3 then -- Continuing this combo move
				P1_combo_move_step[60] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[60] == 4 then -- Continuing this combo move
				P1_combo_move_step[60] = 5
				AIButtons["P1 Up"].pressed = true				
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[60] == 5 then -- Continuing this combo move
				P1_combo_move_step[60] = 6
			elseif P1_combo_move_step[60] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 61 then -- down, down-left, left, up-left + kick
			if P1_combo_move_step[61] == 1 then -- Starting a new combo move
				P1_combo_move_step[61] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[61] == 2 then -- Continuing this combo move
				P1_combo_move_step[61] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[61] == 3 then -- Continuing this combo move
				P1_combo_move_step[61] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[61] == 4 then -- Continuing this combo move
				P1_combo_move_step[61] = 5
				AIButtons["P1 Up"].pressed = true				
				AIButtons["P1 Left"].pressed = true
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[61] == 5 then -- Continuing this combo move
				P1_combo_move_step[61] = 6
			elseif P1_combo_move_step[61] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 62 then -- fireball right + kick
			if P1_combo_move_step[62] == 1 then -- Starting a new combo move
				P1_combo_move_step[62] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[62] == 2 then -- Continuing this combo move
				P1_combo_move_step[62] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[62] == 3 then -- Continuing this combo move
				P1_combo_move_step[62] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[62] == 4 then -- Continuing this combo move
				P1_combo_move_step[62] = 5
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[62] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 63 then -- fireball right + kick
			if P1_combo_move_step[63] == 1 then -- Starting a new combo move
				P1_combo_move_step[63] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[63] == 2 then -- Continuing this combo move
				P1_combo_move_step[63] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[63] == 3 then -- Continuing this combo move
				P1_combo_move_step[63] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[63] == 4 then -- Continuing this combo move
				P1_combo_move_step[63] = 5
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[63] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 64 then -- fireball right + kick
			if P1_combo_move_step[64] == 1 then -- Starting a new combo move
				P1_combo_move_step[64] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[64] == 2 then -- Continuing this combo move
				P1_combo_move_step[64] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[64] == 3 then -- Continuing this combo move
				P1_combo_move_step[64] = 4
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[64] == 4 then -- Continuing this combo move
				P1_combo_move_step[64] = 5
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[64] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 65 then -- fireball left + kick
			if P1_combo_move_step[65] == 1 then -- Starting a new combo move
				P1_combo_move_step[65] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[65] == 2 then -- Continuing this combo move
				P1_combo_move_step[65] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[65] == 3 then -- Continuing this combo move
				P1_combo_move_step[65] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[65] == 4 then -- Continuing this combo move
				P1_combo_move_step[65] = 5
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[65] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 66 then -- fireball left + kick
			if P1_combo_move_step[66] == 1 then -- Starting a new combo move
				P1_combo_move_step[66] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[66] == 2 then -- Continuing this combo move
				P1_combo_move_step[66] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[66] == 3 then -- Continuing this combo move
				P1_combo_move_step[66] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[66] == 4 then -- Continuing this combo move
				P1_combo_move_step[66] = 5
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[66] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 67 then -- fireball left + kick
			if P1_combo_move_step[67] == 1 then -- Starting a new combo move
				P1_combo_move_step[67] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[67] == 2 then -- Continuing this combo move
				P1_combo_move_step[67] = 3
				AIButtons["P1 Down"].pressed = true
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[67] == 3 then -- Continuing this combo move
				P1_combo_move_step[67] = 4
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[67] == 4 then -- Continuing this combo move
				P1_combo_move_step[67] = 5
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[67] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 68 then -- Charge left 2 seconds, right + kick (balrog uppercut)
			if P1_combo_move_step[68] == 1 then -- Starting a new combo move
				P1_combo_move_step[68] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[68] >= 2 and P1_combo_move_step[68] <= 35 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				P1_combo_move_step[68] = P1_combo_move_step[68] + 1
			elseif P1_combo_move_step[68] == 36 then -- Continuing this combo move	
				P1_combo_move_step[68] = 37		
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[68] == 37 then -- Continuing this combo move
				P1_combo_move_step[68] = 38		
				AIButtons["P1 Right"].pressed = true	
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[68] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 69 then -- Charge left 2 seconds, right + kick (balrog uppercut)
			if P1_combo_move_step[69] == 1 then -- Starting a new combo move
				P1_combo_move_step[69] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[69] >= 2 and P1_combo_move_step[69] <= 35 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				P1_combo_move_step[69] = P1_combo_move_step[69] + 1
			elseif P1_combo_move_step[69] == 36 then -- Continuing this combo move	
				P1_combo_move_step[69] = 37		
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[69] == 37 then -- Continuing this combo move
				P1_combo_move_step[69] = 38		
				AIButtons["P1 Right"].pressed = true	
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[69] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 70 then -- Charge left 2 seconds, right + kick (balrog uppercut)
			if P1_combo_move_step[70] == 1 then -- Starting a new combo move
				P1_combo_move_step[70] = 2
				AIButtons["P1 Left"].pressed = true
			elseif P1_combo_move_step[70] >= 2 and P1_combo_move_step[70] <= 35 then -- Continuing this combo move
				AIButtons["P1 Left"].pressed = true
				P1_combo_move_step[70] = P1_combo_move_step[70] + 1
			elseif P1_combo_move_step[70] == 36 then -- Continuing this combo move	
				P1_combo_move_step[70] = 37		
				AIButtons["P1 Right"].pressed = true	
			elseif P1_combo_move_step[70] == 37 then -- Continuing this combo move
				P1_combo_move_step[70] = 38		
				AIButtons["P1 Right"].pressed = true	
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[70] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 71 then -- Charge right 2 seconds, left + kick (balrog uppercut)
			if P1_combo_move_step[71] == 1 then -- Starting a new combo move
				P1_combo_move_step[71] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[71] >= 2 and P1_combo_move_step[71] <= 35 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				P1_combo_move_step[71] = P1_combo_move_step[71] + 1
			elseif P1_combo_move_step[71] == 36 then -- Continuing this combo move	
				P1_combo_move_step[71] = 37		
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[71] == 37 then -- Continuing this combo move
				P1_combo_move_step[71] = 38		
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[71] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 72 then -- Charge right 2 seconds, left + kick (balrog uppercut)
			if P1_combo_move_step[72] == 1 then -- Starting a new combo move
				P1_combo_move_step[72] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[72] >= 2 and P1_combo_move_step[72] <= 35 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				P1_combo_move_step[72] = P1_combo_move_step[72] + 1
			elseif P1_combo_move_step[72] == 36 then -- Continuing this combo move	
				P1_combo_move_step[72] = 37		
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[72] == 37 then -- Continuing this combo move
				P1_combo_move_step[72] = 38		
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[72] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 73 then -- Charge right 2 seconds, left + kick (balrog uppercut)
			if P1_combo_move_step[73] == 1 then -- Starting a new combo move
				P1_combo_move_step[73] = 2
				AIButtons["P1 Right"].pressed = true
			elseif P1_combo_move_step[73] >= 2 and P1_combo_move_step[73] <= 35 then -- Continuing this combo move
				AIButtons["P1 Right"].pressed = true
				P1_combo_move_step[73] = P1_combo_move_step[73] + 1
			elseif P1_combo_move_step[73] == 36 then -- Continuing this combo move	
				P1_combo_move_step[73] = 37		
				AIButtons["P1 Left"].pressed = true	
			elseif P1_combo_move_step[73] == 37 then -- Continuing this combo move
				P1_combo_move_step[73] = 38		
				AIButtons["P1 Left"].pressed = true	
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[73] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 74 then -- Charge down 2 seconds, up + kick (guile, chun li, etc)
			if P1_combo_move_step[74] == 1 then -- Starting a new combo move
				P1_combo_move_step[74] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[74] >= 2 and P1_combo_move_step[74] <= 42 then -- Continuing this combo move
				AIButtons["P1 Down"].pressed = true
				P1_combo_move_step[74] = P1_combo_move_step[74] + 1
			elseif P1_combo_move_step[74] == 43 then -- Continuing this combo move
				P1_combo_move_step[74] = 44
				AIButtons["P1 Up"].pressed = true					
				AIButtons["P1 Short Kick"].pressed = true																
			elseif P1_combo_move_step[74] == 44 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 75 then -- Charge down 2 seconds, up + kick (guile, chun li, etc)
			if P1_combo_move_step[75] == 1 then -- Starting a new combo move
				P1_combo_move_step[75] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[75] >= 2 and P1_combo_move_step[75] <= 42 then -- Continuing this combo move
				AIButtons["P1 Down"].pressed = true
				P1_combo_move_step[75] = P1_combo_move_step[75] + 1
			elseif P1_combo_move_step[75] == 43 then -- Continuing this combo move	
				P1_combo_move_step[75] = 44
				AIButtons["P1 Up"].pressed = true	
				AIButtons["P1 Forward Kick"].pressed = true																
			elseif P1_combo_move_step[75] == 44 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 76 then -- Charge down 2 seconds, up + kick (guile, chun li, etc)
			if P1_combo_move_step[76] == 1 then -- Starting a new combo move
				P1_combo_move_step[76] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[76] >= 2 and P1_combo_move_step[76] <= 42 then -- Continuing this combo move
				AIButtons["P1 Down"].pressed = true
				P1_combo_move_step[76] = P1_combo_move_step[76] + 1
			elseif P1_combo_move_step[76] == 43 then -- Continuing this combo move	
				P1_combo_move_step[76] = 44
				AIButtons["P1 Up"].pressed = true	
				AIButtons["P1 Roundhouse Kick"].pressed = true																
			elseif P1_combo_move_step[76] == 44 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 77 then -- Charge down 3 seconds, up + kick (e honda)
			if P1_combo_move_step[77] == 1 then -- Starting a new combo move
				P1_combo_move_step[77] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[77] >= 2 and P1_combo_move_step[77] <= 35 then -- Continuing this combo move
				AIButtons["P1 Down"].pressed = true
				P1_combo_move_step[77] = P1_combo_move_step[77] + 1
			elseif P1_combo_move_step[77] == 36 then -- Continuing this combo move	
				P1_combo_move_step[77] = 37		
				AIButtons["P1 Up"].pressed = true	
			elseif P1_combo_move_step[77] == 37 then -- Continuing this combo move
				P1_combo_move_step[77] = 38		
				AIButtons["P1 Up"].pressed = true	
				AIButtons["P1 Short Kick"].pressed = true
			elseif P1_combo_move_step[77] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 78 then -- Charge down 3 seconds, up + kick (e honda)
			if P1_combo_move_step[78] == 1 then -- Starting a new combo move
				P1_combo_move_step[78] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[78] >= 2 and P1_combo_move_step[78] <= 35 then -- Continuing this combo move
				AIButtons["P1 Down"].pressed = true
				P1_combo_move_step[78] = P1_combo_move_step[78] + 1
			elseif P1_combo_move_step[78] == 36 then -- Continuing this combo move	
				P1_combo_move_step[78] = 37		
				AIButtons["P1 Up"].pressed = true	
			elseif P1_combo_move_step[78] == 37 then -- Continuing this combo move
				P1_combo_move_step[78] = 38		
				AIButtons["P1 Up"].pressed = true	
				AIButtons["P1 Forward Kick"].pressed = true
			elseif P1_combo_move_step[78] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 79 then -- Charge down 3 seconds, up + kick (e honda)
			if P1_combo_move_step[79] == 1 then -- Starting a new combo move
				P1_combo_move_step[79] = 2
				AIButtons["P1 Down"].pressed = true
			elseif P1_combo_move_step[79] >= 2 and P1_combo_move_step[79] <= 35 then -- Continuing this combo move
				AIButtons["P1 Down"].pressed = true
				P1_combo_move_step[79] = P1_combo_move_step[79] + 1
			elseif P1_combo_move_step[79] == 36 then -- Continuing this combo move	
				P1_combo_move_step[79] = 37		
				AIButtons["P1 Up"].pressed = true	
			elseif P1_combo_move_step[79] == 37 then -- Continuing this combo move
				P1_combo_move_step[79] = 38		
				AIButtons["P1 Up"].pressed = true	
				AIButtons["P1 Roundhouse Kick"].pressed = true
			elseif P1_combo_move_step[79] == 38 then -- Cooldown frame		
				alldone = true		
			end

		end -- index block




		-- If we have finished a combo move, reset all combo move steps
		if alldone then
			for i = combo_index_start, combo_index_end do
			P1_combo_move_step[i] = -1
			end
		end



	else -- Player 2 moves
		alldone = false
		if index == 11 then -- fireball right
			if P2_combo_move_step[11] == 1 then -- Starting a new combo move
				P2_combo_move_step[11] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[11] == 2 then -- Continuing this combo move
				P2_combo_move_step[11] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[11] == 3 then -- Continuing this combo move
				P2_combo_move_step[11] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[11] == 4 then -- Continuing this combo move
				P2_combo_move_step[11] = 5
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[11] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 12 then -- dragon punch right
			if P2_combo_move_step[12] == 1 then -- Starting a new combo move
				P2_combo_move_step[12] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[12] == 2 then -- Continuing this combo move
				P2_combo_move_step[12] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[12] == 3 then -- Continuing this combo move
				P2_combo_move_step[12] = 4
				AIButtons["P2 Down"].pressed = true				
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[12] == 4 then -- Continuing this combo move
				P2_combo_move_step[12] = 5
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[12] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 13 then -- hurricane kick right
			if P2_combo_move_step[13] == 1 then -- Starting a new combo move
				P2_combo_move_step[13] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[13] == 2 then -- Continuing this combo move
				P2_combo_move_step[13] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true				
			elseif P2_combo_move_step[13] == 3 then -- Continuing this combo move
				P2_combo_move_step[13] = 4
				AIButtons["P2 Left"].pressed = true				
			elseif P2_combo_move_step[13] == 4 then -- Continuing this combo move
				P2_combo_move_step[13] = 5
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[13] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 14 then -- fireball left
			if P2_combo_move_step[14] == 1 then -- Starting a new combo move
				P2_combo_move_step[14] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[14] == 2 then -- Continuing this combo move
				P2_combo_move_step[14] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[14] == 3 then -- Continuing this combo move
				P2_combo_move_step[14] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[14] == 4 then -- Continuing this combo move
				P2_combo_move_step[14] = 5
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[14] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 15 then -- dragon punch left
			if P2_combo_move_step[15] == 1 then -- Starting a new combo move
				P2_combo_move_step[15] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[15] == 2 then -- Continuing this combo move
				P2_combo_move_step[15] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[15] == 3 then -- Continuing this combo move
				P2_combo_move_step[15] = 4
				AIButtons["P2 Down"].pressed = true				
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[15] == 4 then -- Continuing this combo move
				P2_combo_move_step[15] = 5
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[15] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 16 then -- hurricane kick left
			if P2_combo_move_step[16] == 1 then -- Starting a new combo move
				P2_combo_move_step[16] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[16] == 2 then -- Continuing this combo move
				P2_combo_move_step[16] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true				
			elseif P2_combo_move_step[16] == 3 then -- Continuing this combo move
				P2_combo_move_step[16] = 4
				AIButtons["P2 Right"].pressed = true				
			elseif P2_combo_move_step[16] == 4 then -- Continuing this combo move
				P2_combo_move_step[16] = 5
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[16] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 17 then -- fireball right
			if P2_combo_move_step[17] == 1 then -- Starting a new combo move
				P2_combo_move_step[17] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[17] == 2 then -- Continuing this combo move
				P2_combo_move_step[17] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[17] == 3 then -- Continuing this combo move
				P2_combo_move_step[17] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[17] == 4 then -- Continuing this combo move
				P2_combo_move_step[17] = 5
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[17] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 18 then -- dragon punch right
			if P2_combo_move_step[18] == 1 then -- Starting a new combo move
				P2_combo_move_step[18] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[18] == 2 then -- Continuing this combo move
				P2_combo_move_step[18] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[18] == 3 then -- Continuing this combo move
				P2_combo_move_step[18] = 4
				AIButtons["P2 Down"].pressed = true				
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[18] == 4 then -- Continuing this combo move
				P2_combo_move_step[18] = 5
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[18] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 19 then -- hurricane kick right
			if P2_combo_move_step[19] == 1 then -- Starting a new combo move
				P2_combo_move_step[19] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[19] == 2 then -- Continuing this combo move
				P2_combo_move_step[19] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true				
			elseif P2_combo_move_step[19] == 3 then -- Continuing this combo move
				P2_combo_move_step[19] = 4
				AIButtons["P2 Left"].pressed = true				
			elseif P2_combo_move_step[19] == 4 then -- Continuing this combo move
				P2_combo_move_step[19] = 5
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[19] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 20 then -- fireball left
			if P2_combo_move_step[20] == 1 then -- Starting a new combo move
				P2_combo_move_step[20] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[20] == 2 then -- Continuing this combo move
				P2_combo_move_step[20] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[20] == 3 then -- Continuing this combo move
				P2_combo_move_step[20] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[20] == 4 then -- Continuing this combo move
				P2_combo_move_step[20] = 5
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[20] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 21 then -- dragon punch left
			if P2_combo_move_step[21] == 1 then -- Starting a new combo move
				P2_combo_move_step[21] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[21] == 2 then -- Continuing this combo move
				P2_combo_move_step[21] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[21] == 3 then -- Continuing this combo move
				P2_combo_move_step[21] = 4
				AIButtons["P2 Down"].pressed = true				
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[21] == 4 then -- Continuing this combo move
				P2_combo_move_step[21] = 5
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[21] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 22 then -- hurricane kick left
			if P2_combo_move_step[22] == 1 then -- Starting a new combo move
				P2_combo_move_step[22] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[22] == 2 then -- Continuing this combo move
				P2_combo_move_step[22] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true				
			elseif P2_combo_move_step[22] == 3 then -- Continuing this combo move
				P2_combo_move_step[22] = 4
				AIButtons["P2 Right"].pressed = true				
			elseif P2_combo_move_step[22] == 4 then -- Continuing this combo move
				P2_combo_move_step[22] = 5
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[22] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 23 then -- fireball right
			if P2_combo_move_step[23] == 1 then -- Starting a new combo move
				P2_combo_move_step[23] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[23] == 2 then -- Continuing this combo move
				P2_combo_move_step[23] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[23] == 3 then -- Continuing this combo move
				P2_combo_move_step[23] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[23] == 4 then -- Continuing this combo move
				P2_combo_move_step[23] = 5
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[23] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 24 then -- dragon punch right
			if P2_combo_move_step[24] == 1 then -- Starting a new combo move
				P2_combo_move_step[24] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[24] == 2 then -- Continuing this combo move
				P2_combo_move_step[24] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[24] == 3 then -- Continuing this combo move
				P2_combo_move_step[24] = 4
				AIButtons["P2 Down"].pressed = true				
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[24] == 4 then -- Continuing this combo move
				P2_combo_move_step[24] = 5
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[24] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 25 then -- hurricane kick right
			if P2_combo_move_step[25] == 1 then -- Starting a new combo move
				P2_combo_move_step[25] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[25] == 2 then -- Continuing this combo move
				P2_combo_move_step[25] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true				
			elseif P2_combo_move_step[25] == 3 then -- Continuing this combo move
				P2_combo_move_step[25] = 4
				AIButtons["P2 Left"].pressed = true				
			elseif P2_combo_move_step[25] == 4 then -- Continuing this combo move
				P2_combo_move_step[25] = 5
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[25] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 26 then -- fireball left
			if P2_combo_move_step[26] == 1 then -- Starting a new combo move
				P2_combo_move_step[26] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[26] == 2 then -- Continuing this combo move
				P2_combo_move_step[26] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[26] == 3 then -- Continuing this combo move
				P2_combo_move_step[26] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[26] == 4 then -- Continuing this combo move
				P2_combo_move_step[26] = 5
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[26] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 27 then -- dragon punch left
			if P2_combo_move_step[27] == 1 then -- Starting a new combo move
				P2_combo_move_step[27] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[27] == 2 then -- Continuing this combo move
				P2_combo_move_step[27] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[27] == 3 then -- Continuing this combo move
				P2_combo_move_step[27] = 4
				AIButtons["P2 Down"].pressed = true				
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[27] == 4 then -- Continuing this combo move
				P2_combo_move_step[27] = 5
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[27] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 28 then -- hurricane kick left
			if P2_combo_move_step[28] == 1 then -- Starting a new combo move
				P2_combo_move_step[28] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[28] == 2 then -- Continuing this combo move
				P2_combo_move_step[28] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true				
			elseif P2_combo_move_step[28] == 3 then -- Continuing this combo move
				P2_combo_move_step[28] = 4
				AIButtons["P2 Right"].pressed = true				
			elseif P2_combo_move_step[28] == 4 then -- Continuing this combo move
				P2_combo_move_step[28] = 5
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[28] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 29 then -- Charge left 2 seconds, right + jab punch (guile sonic boom, etc)
			if P2_combo_move_step[29] == 1 then -- Starting a new combo move
				P2_combo_move_step[29] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[29] >= 2 and P2_combo_move_step[29] <= 35 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				P2_combo_move_step[29] = P2_combo_move_step[29] + 1
			elseif P2_combo_move_step[29] == 36 then -- Continuing this combo move
				P2_combo_move_step[29] = 37
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[29] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P2_combo_move_step[29] = 38							
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[29] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 30 then -- Charge left 2 seconds, right + strong punch (guile sonic boom, etc)
			if P2_combo_move_step[30] == 1 then -- Starting a new combo move
				P2_combo_move_step[30] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[30] >= 2 and P2_combo_move_step[30] <= 35 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				P2_combo_move_step[30] = P2_combo_move_step[30] + 1
			elseif P2_combo_move_step[30] == 36 then -- Continuing this combo move
				P2_combo_move_step[30] = 37
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[30] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P2_combo_move_step[30] = 38							
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[30] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 31 then -- Charge left 2 seconds, right + fierce punch (guile sonic boom, etc)
			if P2_combo_move_step[31] == 1 then -- Starting a new combo move
				P2_combo_move_step[31] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[31] >= 2 and P2_combo_move_step[31] <= 35 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				P2_combo_move_step[31] = P2_combo_move_step[31] + 1
			elseif P2_combo_move_step[31] == 36 then -- Continuing this combo move
				P2_combo_move_step[31] = 37
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[31] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P2_combo_move_step[31] = 38							
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[31] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 32 then -- Charge right 2 seconds, left + jab punch (guile sonic boom, etc)
			if P2_combo_move_step[32] == 1 then -- Starting a new combo move
				P2_combo_move_step[32] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[32] >= 2 and P2_combo_move_step[32] <= 35 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				P2_combo_move_step[32] = P2_combo_move_step[32] + 1
			elseif P2_combo_move_step[32] == 36 then -- Continuing this combo move
				P2_combo_move_step[32] = 37
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[32] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P2_combo_move_step[32] = 38							
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[32] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 33 then -- Charge right 2 seconds, left + strong punch (guile sonic boom, etc)
			if P2_combo_move_step[33] == 1 then -- Starting a new combo move
				P2_combo_move_step[33] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[33] >= 2 and P2_combo_move_step[33] <= 35 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				P2_combo_move_step[33] = P2_combo_move_step[33] + 1
			elseif P2_combo_move_step[33] == 36 then -- Continuing this combo move
				P2_combo_move_step[33] = 37
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[33] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P2_combo_move_step[33] = 38								
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[33] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 34 then -- Charge right 2 seconds, left + fierce punch (guile sonic boom, etc)
			if P2_combo_move_step[34] == 1 then -- Starting a new combo move
				P2_combo_move_step[34] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[34] >= 2 and P2_combo_move_step[34] <= 35 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				P2_combo_move_step[34] = P2_combo_move_step[34] + 1
			elseif P2_combo_move_step[34] == 36 then -- Continuing this combo move
				P2_combo_move_step[34] = 37
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[34] == 37 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick	
				P2_combo_move_step[34] = 38							
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[34] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 35 then -- Charge left+down 2 seconds, right + jab punch (guile sonic boom, etc)
			if P2_combo_move_step[35] == 1 then -- Starting a new combo move
				P2_combo_move_step[35] = 2
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[35] >= 2 and P2_combo_move_step[35] <= 31 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Down"].pressed = true				
				P2_combo_move_step[35] = P2_combo_move_step[35] + 1
			elseif P2_combo_move_step[35] == 32 then -- Continuing this combo move
				P2_combo_move_step[35] = 33
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[35] == 33 then -- Continuing this combo move	
				P2_combo_move_step[35] = 34		
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[35] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P2_combo_move_step[35] = 35		
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[35] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 36 then -- Charge left+down 2 seconds, right + strong punch (guile sonic boom, etc)
			if P2_combo_move_step[36] == 1 then -- Starting a new combo move
				P2_combo_move_step[36] = 2
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[36] >= 2 and P2_combo_move_step[36] <= 31 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Down"].pressed = true				
				P2_combo_move_step[36] = P2_combo_move_step[36] + 1
			elseif P2_combo_move_step[36] == 32 then -- Continuing this combo move
				P2_combo_move_step[36] = 33
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[36] == 33 then -- Continuing this combo move	
				P2_combo_move_step[36] = 34		
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[36] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P2_combo_move_step[36] = 35		
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[36] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 37 then -- Charge left+down 2 seconds, right + fierce punch (guile sonic boom, etc)
			if P2_combo_move_step[37] == 1 then -- Starting a new combo move
				P2_combo_move_step[37] = 2
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[37] >= 2 and P2_combo_move_step[37] <= 31 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Down"].pressed = true				
				P2_combo_move_step[37] = P2_combo_move_step[37] + 1
			elseif P2_combo_move_step[37] == 32 then -- Continuing this combo move
				P2_combo_move_step[37] = 33
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[37] == 33 then -- Continuing this combo move	
				P2_combo_move_step[37] = 34		
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[37] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P2_combo_move_step[37] = 35		
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[37] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 38 then -- Charge right+down 2 seconds, left + jab punch (guile sonic boom, etc)
			if P2_combo_move_step[38] == 1 then -- Starting a new combo move
				P2_combo_move_step[38] = 2
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[38] >= 2 and P2_combo_move_step[38] <= 31 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Down"].pressed = true				
				P2_combo_move_step[38] = P2_combo_move_step[38] + 1
			elseif P2_combo_move_step[38] == 32 then -- Continuing this combo move
				P2_combo_move_step[38] = 33
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[38] == 33 then -- Continuing this combo move	
				P2_combo_move_step[38] = 34		
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[38] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P2_combo_move_step[38] = 35		
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[38] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 39 then -- Charge right+down 2 seconds, left + strong punch (guile sonic boom, etc)
			if P2_combo_move_step[39] == 1 then -- Starting a new combo move
				P2_combo_move_step[39] = 2
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[39] >= 2 and P2_combo_move_step[39] <= 31 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Down"].pressed = true				
				P2_combo_move_step[39] = P2_combo_move_step[39] + 1
			elseif P2_combo_move_step[39] == 32 then -- Continuing this combo move
				P2_combo_move_step[39] = 33
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[39] == 33 then -- Continuing this combo move	
				P2_combo_move_step[39] = 34		
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[39] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P2_combo_move_step[39] = 35		
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[39] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 40 then -- Charge right+down 2 seconds, left + fierce punch (guile sonic boom, etc)
			if P2_combo_move_step[40] == 1 then -- Starting a new combo move
				P2_combo_move_step[40] = 2
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[40] >= 2 and P2_combo_move_step[40] <= 31 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Down"].pressed = true				
				P2_combo_move_step[40] = P2_combo_move_step[40] + 1
			elseif P2_combo_move_step[40] == 32 then -- Continuing this combo move
				P2_combo_move_step[40] = 33
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[40] == 33 then -- Continuing this combo move	
				P2_combo_move_step[40] = 34		
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[40] == 34 then -- E Honda, Blanka are finicky and need an extra frame of forward joystick
				P2_combo_move_step[40] = 35		
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[40] == 35 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 41 then -- Rapidly press punch
			if P2_combo_move_step[41] == 1 then -- Starting a new combo move
				P2_combo_move_step[41] = 2
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[41] >= 2 and P2_combo_move_step[41] <= 59 then -- Continuing this combo move
				if P2_combo_move_step[41] % 2 == 0 then -- Only press every other frame
					AIButtons["P2 Jab Punch"].pressed = true
				end
				P2_combo_move_step[41] = P2_combo_move_step[41] + 1
			elseif P2_combo_move_step[41] == 60 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 42 then -- Rapidly press kick
			if P2_combo_move_step[42] == 1 then -- Starting a new combo move
				P2_combo_move_step[42] = 2
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[42] >= 2 and P2_combo_move_step[42] <= 59 then -- Continuing this combo move
				if P2_combo_move_step[42] % 2 == 0 then -- Only press every other frame
					AIButtons["P2 Short Kick"].pressed = true
				end
				P2_combo_move_step[42] = P2_combo_move_step[42] + 1
			elseif P2_combo_move_step[42] == 60 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 43 then -- All 3 punches
			if P2_combo_move_step[43] >= 1 and P2_combo_move_step[43] <= 4 then -- 4 frames are required to register
				AIButtons["P2 Jab Punch"].pressed = true
				AIButtons["P2 Strong Punch"].pressed = true
				AIButtons["P2 Fierce Punch"].pressed = true
				P2_combo_move_step[43] = P2_combo_move_step[43] + 1
			elseif P2_combo_move_step[43] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 44 then -- All 3 kicks
			if P2_combo_move_step[44] >= 1 and P2_combo_move_step[44] <= 4 then -- 4 frames are required to register
				AIButtons["P2 Short Kick"].pressed = true
				AIButtons["P2 Forward Kick"].pressed = true
				AIButtons["P2 Roundhouse Kick"].pressed = true
				P2_combo_move_step[44] = P2_combo_move_step[44] + 1				
			elseif P2_combo_move_step[44] == 5 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 45 then -- All 3 punches
			if P2_combo_move_step[45] >= 1 and P2_combo_move_step[45] <= 30 then -- 1 second charge
				AIButtons["P2 Jab Punch"].pressed = true
				AIButtons["P2 Strong Punch"].pressed = true
				AIButtons["P2 Fierce Punch"].pressed = true
				P2_combo_move_step[45] = P2_combo_move_step[45] + 1
			elseif P2_combo_move_step[45] == 31 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 46 then -- 360 clockwise (forward, down, back, up + punch)
			if P2_combo_move_step[46] == 1 then -- Starting a new combo move
				P2_combo_move_step[46] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[46] == 2 then -- Continuing this combo move
				P2_combo_move_step[46] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[46] == 3 then -- Continuing this combo move
				P2_combo_move_step[46] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[46] == 4 then -- Continuing this combo move
				P2_combo_move_step[46] = 5
				AIButtons["P2 Up"].pressed = true
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[46] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 47 then -- 360 counter-clockwise (forward, down, back, up + punch)
			if P2_combo_move_step[47] == 1 then -- Starting a new combo move
				P2_combo_move_step[47] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[47] == 2 then -- Continuing this combo move
				P2_combo_move_step[47] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[47] == 3 then -- Continuing this combo move
				P2_combo_move_step[47] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[47] == 4 then -- Continuing this combo move
				P2_combo_move_step[47] = 5
				AIButtons["P2 Up"].pressed = true
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[47] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 48 then -- dragon punch left + all 3 punches
			if P2_combo_move_step[48] == 1 then -- Starting a new combo move
				P2_combo_move_step[48] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[48] == 2 then -- Continuing this combo move
				P2_combo_move_step[48] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[48] == 3 then -- Continuing this combo move
				P2_combo_move_step[48] = 4
				AIButtons["P2 Down"].pressed = true				
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[48] == 4 then -- Continuing this combo move
				P2_combo_move_step[48] = 5
				AIButtons["P2 Jab Punch"].pressed = true
				AIButtons["P2 Strong Punch"].pressed = true
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[48] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 49 then -- dragon punch right + all 3 punches
			if P2_combo_move_step[49] == 1 then -- Starting a new combo move
				P2_combo_move_step[49] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[49] == 2 then -- Continuing this combo move
				P2_combo_move_step[49] = 3
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[49] == 3 then -- Continuing this combo move
				P2_combo_move_step[49] = 4
				AIButtons["P2 Down"].pressed = true				
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[49] == 4 then -- Continuing this combo move
				P2_combo_move_step[49] = 5
				AIButtons["P2 Jab Punch"].pressed = true
				AIButtons["P2 Strong Punch"].pressed = true
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[49] == 5 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 50 then -- half circle fireball right
			if P2_combo_move_step[50] == 1 then -- Starting a new combo move
				P2_combo_move_step[50] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[50] == 2 then -- Continuing this combo move
				P2_combo_move_step[50] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[50] == 3 then -- Continuing this combo move
				P2_combo_move_step[50] = 4
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[50] == 4 then -- Continuing this combo move
				P2_combo_move_step[50] = 5
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[50] == 5 then -- Continuing this combo move
				P2_combo_move_step[50] = 6
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[50] == 6 then -- Continuing this combo move
				P2_combo_move_step[50] = 7
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[50] == 7 then -- Cooldown frame
				alldone = true
			end


		elseif index == 51 then -- half circle fireball right
			if P2_combo_move_step[51] == 1 then -- Starting a new combo move
				P2_combo_move_step[51] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[51] == 2 then -- Continuing this combo move
				P2_combo_move_step[51] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[51] == 3 then -- Continuing this combo move
				P2_combo_move_step[51] = 4
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[51] == 4 then -- Continuing this combo move
				P2_combo_move_step[51] = 5
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[51] == 5 then -- Continuing this combo move
				P2_combo_move_step[51] = 6
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[51] == 6 then -- Continuing this combo move
				P2_combo_move_step[51] = 7
				AIButtons["P2 Strong Punch"].pressed = true
			elseif P2_combo_move_step[51] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 52 then -- half circle fireball right
			if P2_combo_move_step[52] == 1 then -- Starting a new combo move
				P2_combo_move_step[52] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[52] == 2 then -- Continuing this combo move
				P2_combo_move_step[52] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[52] == 3 then -- Continuing this combo move
				P2_combo_move_step[52] = 4
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[52] == 4 then -- Continuing this combo move
				P2_combo_move_step[52] = 5
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[52] == 5 then -- Continuing this combo move
				P2_combo_move_step[52] = 6
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[52] == 6 then -- Continuing this combo move
				P2_combo_move_step[52] = 7
				AIButtons["P2 Fierce Punch"].pressed = true
			elseif P2_combo_move_step[52] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 53 then -- half circle fireball left
			if P2_combo_move_step[53] == 1 then -- Starting a new combo move
				P2_combo_move_step[53] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[53] == 2 then -- Continuing this combo move
				P2_combo_move_step[53] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[53] == 3 then -- Continuing this combo move
				P2_combo_move_step[53] = 4
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[53] == 4 then -- Continuing this combo move
				P2_combo_move_step[53] = 5
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[53] == 5 then -- Continuing this combo move
				P2_combo_move_step[53] = 6
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[53] == 6 then -- Continuing this combo move
				P2_combo_move_step[53] = 7
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[53] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 54 then -- half circle fireball left
			if P2_combo_move_step[54] == 1 then -- Starting a new combo move
				P2_combo_move_step[54] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[54] == 2 then -- Continuing this combo move
				P2_combo_move_step[54] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[54] == 3 then -- Continuing this combo move
				P2_combo_move_step[54] = 4
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[54] == 4 then -- Continuing this combo move
				P2_combo_move_step[54] = 5
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[54] == 5 then -- Continuing this combo move
				P2_combo_move_step[54] = 6
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[54] == 6 then -- Continuing this combo move
				P2_combo_move_step[54] = 7
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[54] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 55 then -- half circle fireball left
			if P2_combo_move_step[55] == 1 then -- Starting a new combo move
				P2_combo_move_step[55] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[55] == 2 then -- Continuing this combo move
				P2_combo_move_step[55] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[55] == 3 then -- Continuing this combo move
				P2_combo_move_step[55] = 4
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[55] == 4 then -- Continuing this combo move
				P2_combo_move_step[55] = 5
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[55] == 5 then -- Continuing this combo move
				P2_combo_move_step[55] = 6
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[55] == 6 then -- Continuing this combo move
				P2_combo_move_step[55] = 7
				AIButtons["P2 Jab Punch"].pressed = true
			elseif P2_combo_move_step[55] == 7 then -- Cooldown frame
				alldone = true
			end

		elseif index == 56 then -- down, down-right, right, up-right + kick
			if P2_combo_move_step[56] == 1 then -- Starting a new combo move
				P2_combo_move_step[56] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[56] == 2 then -- Continuing this combo move
				P2_combo_move_step[56] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[56] == 3 then -- Continuing this combo move
				P2_combo_move_step[56] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[56] == 4 then -- Continuing this combo move
				P2_combo_move_step[56] = 5
				AIButtons["P2 Up"].pressed = true				
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[56] == 5 then -- Continuing this combo move
				P2_combo_move_step[56] = 6
			elseif P2_combo_move_step[56] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 57 then -- down, down-right, right, up-right + kick
			if P2_combo_move_step[57] == 1 then -- Starting a new combo move
				P2_combo_move_step[57] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[57] == 2 then -- Continuing this combo move
				P2_combo_move_step[57] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[57] == 3 then -- Continuing this combo move
				P2_combo_move_step[57] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[57] == 4 then -- Continuing this combo move
				P2_combo_move_step[57] = 5
				AIButtons["P2 Up"].pressed = true				
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[57] == 5 then -- Continuing this combo move
				P2_combo_move_step[57] = 6
			elseif P2_combo_move_step[57] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 58 then -- down, down-right, right, up-right + kick
			if P2_combo_move_step[58] == 1 then -- Starting a new combo move
				P2_combo_move_step[58] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[58] == 2 then -- Continuing this combo move
				P2_combo_move_step[58] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[58] == 3 then -- Continuing this combo move
				P2_combo_move_step[58] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[58] == 4 then -- Continuing this combo move
				P2_combo_move_step[58] = 5
				AIButtons["P2 Up"].pressed = true				
				AIButtons["P2 Right"].pressed = true
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[58] == 5 then -- Continuing this combo move
				P2_combo_move_step[58] = 6
			elseif P2_combo_move_step[58] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 59 then -- down, down-left, left, up-left + kick
			if P2_combo_move_step[59] == 1 then -- Starting a new combo move
				P2_combo_move_step[59] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[59] == 2 then -- Continuing this combo move
				P2_combo_move_step[59] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[59] == 3 then -- Continuing this combo move
				P2_combo_move_step[59] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[59] == 4 then -- Continuing this combo move
				P2_combo_move_step[59] = 5
				AIButtons["P2 Up"].pressed = true				
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[59] == 5 then -- Continuing this combo move
				P2_combo_move_step[59] = 6
			elseif P2_combo_move_step[59] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 60 then -- down, down-left, left, up-left + kick
			if P2_combo_move_step[60] == 1 then -- Starting a new combo move
				P2_combo_move_step[60] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[60] == 2 then -- Continuing this combo move
				P2_combo_move_step[60] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[60] == 3 then -- Continuing this combo move
				P2_combo_move_step[60] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[60] == 4 then -- Continuing this combo move
				P2_combo_move_step[60] = 5
				AIButtons["P2 Up"].pressed = true				
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[60] == 5 then -- Continuing this combo move
				P2_combo_move_step[60] = 6
			elseif P2_combo_move_step[60] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 61 then -- down, down-left, left, up-left + kick
			if P2_combo_move_step[61] == 1 then -- Starting a new combo move
				P2_combo_move_step[61] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[61] == 2 then -- Continuing this combo move
				P2_combo_move_step[61] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[61] == 3 then -- Continuing this combo move
				P2_combo_move_step[61] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[61] == 4 then -- Continuing this combo move
				P2_combo_move_step[61] = 5
				AIButtons["P2 Up"].pressed = true				
				AIButtons["P2 Left"].pressed = true
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[61] == 5 then -- Continuing this combo move
				P2_combo_move_step[61] = 6
			elseif P2_combo_move_step[61] == 6 then -- Cooldown frame	
				alldone = true
			end

		elseif index == 62 then -- fireball right + kick
			if P2_combo_move_step[62] == 1 then -- Starting a new combo move
				P2_combo_move_step[62] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[62] == 2 then -- Continuing this combo move
				P2_combo_move_step[62] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[62] == 3 then -- Continuing this combo move
				P2_combo_move_step[62] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[62] == 4 then -- Continuing this combo move
				P2_combo_move_step[62] = 5
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[62] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 63 then -- fireball right + kick
			if P2_combo_move_step[63] == 1 then -- Starting a new combo move
				P2_combo_move_step[63] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[63] == 2 then -- Continuing this combo move
				P2_combo_move_step[63] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[63] == 3 then -- Continuing this combo move
				P2_combo_move_step[63] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[63] == 4 then -- Continuing this combo move
				P2_combo_move_step[63] = 5
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[63] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 64 then -- fireball right + kick
			if P2_combo_move_step[64] == 1 then -- Starting a new combo move
				P2_combo_move_step[64] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[64] == 2 then -- Continuing this combo move
				P2_combo_move_step[64] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[64] == 3 then -- Continuing this combo move
				P2_combo_move_step[64] = 4
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[64] == 4 then -- Continuing this combo move
				P2_combo_move_step[64] = 5
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[64] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 65 then -- fireball left + kick
			if P2_combo_move_step[65] == 1 then -- Starting a new combo move
				P2_combo_move_step[65] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[65] == 2 then -- Continuing this combo move
				P2_combo_move_step[65] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[65] == 3 then -- Continuing this combo move
				P2_combo_move_step[65] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[65] == 4 then -- Continuing this combo move
				P2_combo_move_step[65] = 5
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[65] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 66 then -- fireball left + kick
			if P2_combo_move_step[66] == 1 then -- Starting a new combo move
				P2_combo_move_step[66] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[66] == 2 then -- Continuing this combo move
				P2_combo_move_step[66] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[66] == 3 then -- Continuing this combo move
				P2_combo_move_step[66] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[66] == 4 then -- Continuing this combo move
				P2_combo_move_step[66] = 5
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[66] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 67 then -- fireball left + kick
			if P2_combo_move_step[67] == 1 then -- Starting a new combo move
				P2_combo_move_step[67] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[67] == 2 then -- Continuing this combo move
				P2_combo_move_step[67] = 3
				AIButtons["P2 Down"].pressed = true
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[67] == 3 then -- Continuing this combo move
				P2_combo_move_step[67] = 4
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[67] == 4 then -- Continuing this combo move
				P2_combo_move_step[67] = 5
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[67] == 5 then -- Cooldown frame
				alldone = true
			end

		elseif index == 68 then -- Charge left 2 seconds, right + kick (balrog uppercut)
			if P2_combo_move_step[68] == 1 then -- Starting a new combo move
				P2_combo_move_step[68] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[68] >= 2 and P2_combo_move_step[68] <= 35 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				P2_combo_move_step[68] = P2_combo_move_step[68] + 1
			elseif P2_combo_move_step[68] == 36 then -- Continuing this combo move	
				P2_combo_move_step[68] = 37		
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[68] == 37 then -- Continuing this combo move
				P2_combo_move_step[68] = 38		
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[68] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 69 then -- Charge left 2 seconds, right + kick (balrog uppercut)
			if P2_combo_move_step[69] == 1 then -- Starting a new combo move
				P2_combo_move_step[69] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[69] >= 2 and P2_combo_move_step[69] <= 35 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				P2_combo_move_step[69] = P2_combo_move_step[69] + 1
			elseif P2_combo_move_step[69] == 36 then -- Continuing this combo move	
				P2_combo_move_step[69] = 37		
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[69] == 37 then -- Continuing this combo move
				P2_combo_move_step[69] = 38		
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[69] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 70 then -- Charge left 2 seconds, right + kick (balrog uppercut)
			if P2_combo_move_step[70] == 1 then -- Starting a new combo move
				P2_combo_move_step[70] = 2
				AIButtons["P2 Left"].pressed = true
			elseif P2_combo_move_step[70] >= 2 and P2_combo_move_step[70] <= 35 then -- Continuing this combo move
				AIButtons["P2 Left"].pressed = true
				P2_combo_move_step[70] = P2_combo_move_step[70] + 1
			elseif P2_combo_move_step[70] == 36 then -- Continuing this combo move	
				P2_combo_move_step[70] = 37		
				AIButtons["P2 Right"].pressed = true	
			elseif P2_combo_move_step[70] == 37 then -- Continuing this combo move
				P2_combo_move_step[70] = 38		
				AIButtons["P2 Right"].pressed = true	
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[70] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 71 then -- Charge right 2 seconds, left + kick (balrog uppercut)
			if P2_combo_move_step[71] == 1 then -- Starting a new combo move
				P2_combo_move_step[71] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[71] >= 2 and P2_combo_move_step[71] <= 35 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				P2_combo_move_step[71] = P2_combo_move_step[71] + 1
			elseif P2_combo_move_step[71] == 36 then -- Continuing this combo move	
				P2_combo_move_step[71] = 37		
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[71] == 37 then -- Continuing this combo move
				P2_combo_move_step[71] = 38		
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[71] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 72 then -- Charge right 2 seconds, left + kick (balrog uppercut)
			if P2_combo_move_step[72] == 1 then -- Starting a new combo move
				P2_combo_move_step[72] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[72] >= 2 and P2_combo_move_step[72] <= 35 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				P2_combo_move_step[72] = P2_combo_move_step[72] + 1
			elseif P2_combo_move_step[72] == 36 then -- Continuing this combo move	
				P2_combo_move_step[72] = 37		
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[72] == 37 then -- Continuing this combo move
				P2_combo_move_step[72] = 38		
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[72] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 73 then -- Charge right 2 seconds, left + kick (balrog uppercut)
			if P2_combo_move_step[73] == 1 then -- Starting a new combo move
				P2_combo_move_step[73] = 2
				AIButtons["P2 Right"].pressed = true
			elseif P2_combo_move_step[73] >= 2 and P2_combo_move_step[73] <= 35 then -- Continuing this combo move
				AIButtons["P2 Right"].pressed = true
				P2_combo_move_step[73] = P2_combo_move_step[73] + 1
			elseif P2_combo_move_step[73] == 36 then -- Continuing this combo move	
				P2_combo_move_step[73] = 37		
				AIButtons["P2 Left"].pressed = true	
			elseif P2_combo_move_step[73] == 37 then -- Continuing this combo move
				P2_combo_move_step[73] = 38		
				AIButtons["P2 Left"].pressed = true	
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[73] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 74 then -- Charge down 2 seconds, up + kick (guile, chun li, etc)
			if P2_combo_move_step[74] == 1 then -- Starting a new combo move
				P2_combo_move_step[74] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[74] >= 2 and P2_combo_move_step[74] <= 42 then -- Continuing this combo move
				AIButtons["P2 Down"].pressed = true
				P2_combo_move_step[74] = P2_combo_move_step[74] + 1
			elseif P2_combo_move_step[74] == 43 then -- Continuing this combo move	
				P2_combo_move_step[74] = 44
				AIButtons["P2 Up"].pressed = true	
				AIButtons["P2 Short Kick"].pressed = true																
			elseif P2_combo_move_step[74] == 44 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 75 then -- Charge down 2 seconds, up + kick (guile, chun li, etc)
			if P2_combo_move_step[75] == 1 then -- Starting a new combo move
				P2_combo_move_step[75] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[75] >= 2 and P2_combo_move_step[75] <= 42 then -- Continuing this combo move
				AIButtons["P2 Down"].pressed = true
				P2_combo_move_step[75] = P2_combo_move_step[75] + 1
			elseif P2_combo_move_step[75] == 43 then -- Continuing this combo move	
				P2_combo_move_step[75] = 44
				AIButtons["P2 Up"].pressed = true	
				AIButtons["P2 Forward Kick"].pressed = true																
			elseif P2_combo_move_step[75] == 44 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 76 then -- Charge down 2 seconds, up + kick (guile, chun li, etc)
			if P2_combo_move_step[76] == 1 then -- Starting a new combo move
				P2_combo_move_step[76] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[76] >= 2 and P2_combo_move_step[76] <= 42 then -- Continuing this combo move
				AIButtons["P2 Down"].pressed = true
				P2_combo_move_step[76] = P2_combo_move_step[76] + 1
			elseif P2_combo_move_step[76] == 43 then -- Continuing this combo move	
				P2_combo_move_step[76] = 44
				AIButtons["P2 Up"].pressed = true	
				AIButtons["P2 Roundhouse Kick"].pressed = true																
			elseif P2_combo_move_step[76] == 44 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 77 then -- Charge down 3 seconds, up + kick (e honda)
			if P2_combo_move_step[77] == 1 then -- Starting a new combo move
				P2_combo_move_step[77] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[77] >= 2 and P2_combo_move_step[77] <= 35 then -- Continuing this combo move
				AIButtons["P2 Down"].pressed = true
				P2_combo_move_step[77] = P2_combo_move_step[77] + 1
			elseif P2_combo_move_step[77] == 36 then -- Continuing this combo move	
				P2_combo_move_step[77] = 37		
				AIButtons["P2 Up"].pressed = true	
			elseif P2_combo_move_step[77] == 37 then -- Continuing this combo move
				P2_combo_move_step[77] = 38		
				AIButtons["P2 Up"].pressed = true	
				AIButtons["P2 Short Kick"].pressed = true
			elseif P2_combo_move_step[77] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 78 then -- Charge down 3 seconds, up + kick (e honda)
			if P2_combo_move_step[78] == 1 then -- Starting a new combo move
				P2_combo_move_step[78] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[78] >= 2 and P2_combo_move_step[78] <= 35 then -- Continuing this combo move
				AIButtons["P2 Down"].pressed = true
				P2_combo_move_step[78] = P2_combo_move_step[78] + 1
			elseif P2_combo_move_step[78] == 36 then -- Continuing this combo move	
				P2_combo_move_step[78] = 37		
				AIButtons["P2 Up"].pressed = true	
			elseif P2_combo_move_step[78] == 37 then -- Continuing this combo move
				P2_combo_move_step[78] = 38		
				AIButtons["P2 Up"].pressed = true	
				AIButtons["P2 Forward Kick"].pressed = true
			elseif P2_combo_move_step[78] == 38 then -- Cooldown frame		
				alldone = true		
			end

		elseif index == 79 then -- Charge down 3 seconds, up + kick (e honda)
			if P2_combo_move_step[79] == 1 then -- Starting a new combo move
				P2_combo_move_step[79] = 2
				AIButtons["P2 Down"].pressed = true
			elseif P2_combo_move_step[79] >= 2 and P2_combo_move_step[79] <= 35 then -- Continuing this combo move
				AIButtons["P2 Down"].pressed = true
				P2_combo_move_step[79] = P2_combo_move_step[79] + 1
			elseif P2_combo_move_step[79] == 36 then -- Continuing this combo move	
				P2_combo_move_step[79] = 37		
				AIButtons["P2 Up"].pressed = true	
			elseif P2_combo_move_step[79] == 37 then -- Continuing this combo move
				P2_combo_move_step[79] = 38		
				AIButtons["P2 Up"].pressed = true	
				AIButtons["P2 Roundhouse Kick"].pressed = true
			elseif P2_combo_move_step[79] == 38 then -- Cooldown frame		
				alldone = true		
			end



		end -- index block


		-- If we have finished a combo move, reset all combo move steps
		if alldone then
			for i = combo_index_start, combo_index_end do
			P2_combo_move_step[i] = -1
			end
		end


	end -- Player 2

end


--
-- ComboMoves:StartMove() - Starts the specified move for the specified player
--
function ComboMoves:StartMove(playernum, index)
	if playernum == 1 then
		P1_combo_move_step[index] = 1
	else
		P2_combo_move_step[index] = 1
	end
end



return ComboMoves