# ANN_sf2hf

### Overview:

ANN_sf2hf is a plugin for the MAME emulator that uses a pair of adversarial neural networks that learn how to play the popular arcade game Street Fighter 2 against one another.  ANN_sf2hf is written in Lua.


### Instructions:

Installation and setup of MAME is beyond the scope of this document.  You will require the "Street Fighter 2: Hyper Fighting (World 921209)"/"sf2hf" ROM file.  No other game version will work as memory addresses differ between versions!

Copy the "ANN_sf2hf" folder into the MAME plugins folder.

To launch MAME with this plugin, use the following command line:
> mame64 sf2hf -w -skip_gameinfo -autoboot_script plugins\ANN_sf2hf\ANN_sf2hf.lua

Use the "-speed N" command line option to run the emulator faster than default.  For example "-speed 6.0" will run at 6x the speed.  Alternatively use "-nothrottle" to run at the maximum possible speed.  Also useful are the "-sound none" and "-video none" command line options which will run the emulator without audio or a display.


### Neural Network/Genetic Algorithm Overview:

Each player is controlled by a neural network that accepts a variety of game data as inputs and outputs joystick movement and button presses.  The inputs to the network include the health of each character, distance between characters, projectiles, and more.  Based on this input, the network will determine whether to activate the joystick, any of the six attack buttons, or a special move. 

At the start, a number of networks (genomes) are created for each player and populated at random.  In order to "learn", a genetic algorithm is used to judge the "fitness" of each genome in the current generation.  Then a number of the most "fit" genomes are selected to act as "parents" to create a new generation of networks.  This process continues to ultimately create networks that can most suit the task - that is to be able to defeat the opposing player.  By pitting two sets of neural networks against each other they will teach one another to be better at playing the game. 


For example, in the first generation, each AI will act completely randomly and may not be effective at all...

<img src="https://gitlab.com/dannmoore/ann_sf2hf/-/raw/master/media/animated1.gif" alt="animated" width="640"/>

Later on they are able to learn which special moves are valid for each character...

<img src="https://gitlab.com/dannmoore/ann_sf2hf/-/raw/master/media/animated2.gif" alt="animated" width="640"/>

After many generations the AI produces a competitive mirror match using Sagat...

<img src="https://gitlab.com/dannmoore/ann_sf2hf/-/raw/master/media/animated3.gif" alt="animated" width="640"/>



### Notes:

- The AI can support any character in the game
- After one player wins two rounds the AI will insert a coin, continue the game and select a new character at random.  The networks will then need to learn how to play using the new combination of opponents.  This requires many playthroughs to learn how to fight both using and how to fight against each opponent.
- The current generation is automatically saved when created, and also upon exit.  Depending on the size of the neural networks it may take hours or even days of gameplay to learn how to be effective.  The genomes will be loaded at next launch so multiple sessions can be used without losing progress.  
- Player 1 and Player 2 may have differently sized and structured networks.  This could give interesting insight into the effectiveness of each network as they learn from each other.
- Many outputs in the neural network were used to assist the AI in performing special attacks.  This is due to the game having not only directional specials (such as the "fireball" motion), but also charge specials (holding back for two seconds, then pushing forward and attack, for example).  It was felt that the AI would otherwise not be able to perform these different attacks reliably, and the goal is not to learn how to perform special moves, but to play the game using them.



### Legal:
Copyright (C) 2024 Dann Moore.  All rights reserved.





